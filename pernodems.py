from tkinter import *
import custom_wid
from tkinter import ttk
import apis
import acq
import random
import entryframe
import tkMessageBox
import threading


######################used to add keys in the database#####################
def addkey():
    key1=ent1.get()
    apis.insert_keys([str(key1),'pernod123','0','0']) ###default db values
    top1.destroy()


######################used to update keys in the database###################
def updatekey():
    key1=ent1.get()
    apis.insert_keys([str(key1),'pernod123','0','0'])
    top1.destroy()


######################checks the wether the db has the the key already######
while(1):
    a=apis.get_keys()
    if a==[]:
        pss='pernod123'
        top1=Tk()
        labl1=Label(top1,text="Enter the activation key")
        labl1.grid(row=0,column=0)
        ent1=Entry(top1,show='*')
        ent1.grid(row=0,column=1)
        but1=Button(top1,text='Ok',command=addkey)
        but1.grid(row=1,column=0,columnspan=2)
        top1.mainloop()
    else:
        get='94384765625'
        keys=apis.get_keys()
        if str(get)==str(keys[0][0]):
            break
        else:
            apis.dropkeys()
            apis.create()
            

##############################################################################################
## The next block fetches all the department names,meter details and meter names
## try is used to ensure that any error occuring during the cycle does not cause code to stop
##############################################################################################
try:    
    departments=apis.fetch_all_deptnames()[0]
except:
    pass
try:
    meters=apis.fetch_all_deptnames()[1]
except:
    pass
try:
    meternames=apis.fetch_all_meternames()
except:
    pass


def parameter_frame():
##fetches and sets the meters for each department
    global frame_class,departments
    frame_class.pack_one(1,[0,0])
    block_combo['values']=departments

    
def view_meter_details():
#views the details of meters
    global meters
    table=entryframe.entry_table(root,len(meters)+1,3)
    table.draw_table()
    table.add_row(1,meters)

    
def get_meter_dept(block_name):
    global meters
    keys=meters[block_name]
##    req_keys=[]
##    for i in range(len(meters)):
##        if (meters[keys[i]]['dept_name'])==block_name:
##            req_keys.append(keys[i])
    return(keys)


def callback1(event):
    name=block_combo.get()
    req_keys=get_meter_dept(name)
    meter_combo['values']=req_keys
def callback2(event):
    name=edm_block_combo1.get()
    req_keys=get_meter_dept(name)
    edm_meter_combo['values']=req_keys
    
    #put this in a diffrent file     
class frame():
    def __init__(self,name,data):
        self._name=name
        self._data=data
    def create_table(self):
        self._window=Toplevel(bg='cyan')
        #self._window.geometry("{0}x{1}+0+0".format(root.winfo_screenwidth(), root.winfo_screenheight()))
        self._mainframe=Frame(self._window,background='cyan')
        self._mainframe.grid(row=0,column=0)
        #self._mainframe.grid_propagate(0)width=root.winfo_screenwidth(),height=root.winfo_screenheight()
        self._head=Label(self._mainframe,text=self._name,font=("helvatica",'24'),width=15,bg='cyan').grid(row=0,column=0,columnspan=5,sticky='n')
        self._inframe=Frame(self._mainframe,background='yellow')
        self._inframe.grid(row=1,column=0)
        self._row_counter=0
        self._headparma=Label(self._inframe,text='Main Parametrs',font=("helvatica",'18'),width=15).grid(row=self._row_counter,column=0,columnspan=5,sticky='n',pady=5)
        #self._inframe.grid_propagate(0)
        self._row_counter+=1
        self._titles=['Parameter','R','Y','B','Avg']
        self._title_second=['Parameters','Total Value']
        self._parameters=['Voltage-L','Voltage-P','Current','Power(KW)','KVA','kVAr','PF']
        self._parameters_second=['Frequency','KWH(Total)','KVAH(Total)','KVArH(Total)']
        titlelbl=[]
        paramlbl=[]
        paramlbl1=[]
        titlelbl1=[]
        self._string_vars=[]
        self._values_lbls=[]
        for i in range(32):
            self._string_vars.append(StringVar())        
        for i in range(0,len(self._titles)):
            titlelbl.append(Label(self._inframe,text=self._titles[i],font=("helvatica",'15'),width=15,bg='White'))
            titlelbl[i].grid(row=self._row_counter,column=i,padx=2)
        self._row_counter+=1
        counter=0
        for i in range(len(self._parameters)):
            paramlbl.append(Label(self._inframe,text=self._parameters[i],font=("helvatica",'15'),width=15,bg='White'))
            self._values_lbls.append(Label(self._inframe,text=self._string_vars[i].get(),font=("helvatica",'15'),width=15,bg='White'))
            self._values_lbls.append(Label(self._inframe,text=self._string_vars[len(self._parameters)+i].get(),font=("helvatica",'15'),width=15,bg='White'))
            self._values_lbls.append(Label(self._inframe,text=self._string_vars[2*len(self._parameters)+i].get(),font=("helvatica",'15'),width=15,bg='White'))
            self._values_lbls.append(Label(self._inframe,text=self._string_vars[3*len(self._parameters)+i].get(),font=("helvatica",'15'),width=15,bg='White'))
            paramlbl[i].grid(row=self._row_counter,column=0,padx=2,pady=2)
            self._values_lbls[counter].grid(row=self._row_counter,column=1,padx=2,pady=2)
            self._values_lbls[counter+1].grid(row=self._row_counter,column=2,padx=2,pady=2)
            self._values_lbls[counter+2].grid(row=self._row_counter,column=3,padx=2,pady=2)
            self._values_lbls[counter+3].grid(row=self._row_counter,column=4,padx=2,pady=2)
            self._row_counter+=1
            counter+=4
        self._second_head=Label(self._inframe,text='Power Parameters',font=("helvatica",'18'),width=15).grid(row=self._row_counter,column=0,columnspan=5,sticky='n',pady=5)
        self._row_counter+=1
        for i in range(0,len(self._title_second)):
            titlelbl1.append(Label(self._inframe,text=self._title_second[i],font=("helvatica",'15'),width=15,bg='White'))
            titlelbl1[i].grid(row=self._row_counter,column=i+1,columnspan=2,padx=2)
        self._row_counter+=1
        counter=28

        for i in range(len(self._parameters_second)):
            paramlbl1.append(Label(self._inframe,text=self._parameters_second[i],font=("helvatica",'15'),width=15,bg='White'))
            self._values_lbls.append(Label(self._inframe,text=self._string_vars[counter].get(),font=("helvatica",'15'),width=15,bg='White'))
            self._values_lbls[len(self._values_lbls)-1].grid(row=self._row_counter,column=2,columnspan=2,padx=2,pady=2)
            paramlbl1[i].grid(row=self._row_counter,column=1,columnspan=2,padx=2,pady=2)
            self._row_counter+=1
            counter+=1

        #self._window.mainloop()
    def set_values(self,values):
        #print(values[21])
        for i in range(0,15):
            self._string_vars[i].set(values[i])
            self._values_lbls[i].config(text=self._string_vars[i].get())
        self._string_vars[15].set(values[21])
        self._values_lbls[15].config(text=self._string_vars[15].get())
        for i in range(16,19):
            self._string_vars[i].set(values[i-1])
            self._values_lbls[i].config(text=self._string_vars[i].get())
        self._string_vars[19].set(values[22])
        self._values_lbls[19].config(text=self._string_vars[19].get())
        for i in range(20,23):
            self._string_vars[i].set(values[i-2])
            self._values_lbls[i].config(text=self._string_vars[i].get())
        self._string_vars[23].set(values[23])
        self._values_lbls[23].config(text=self._string_vars[23].get())
        for i in range(24,33):
            self._string_vars[i].set(values[i])
            self._values_lbls[i].config(text=self._string_vars[i].get())



def normalise(listed,place):
    new_value=[]
    for i in listed:
        try:
            factor=10**place
            temp=abs(float(int(i*(factor)))/(factor))
            new_value.append(temp)
        except:
            new_value.append(i)
    return(new_value)
    
def change_value(frame_name,meter_name):
    all_fetched_values=acq.fetch_values()
    sid=apis.slave_id_from_mname(str(meter_name))
    new_value=normalise(all_fetched_values[str(sid)],4)
    #print(new_value)
    try:
        frame_name.set_values(new_value)   
        root.after(1000,lambda:change_value(frame_name))
    except:
        return(False)

def create_new_frame():
    frame1=frame(meter_combo.get(),['--'])
    frame1.create_table()
    change_value(frame1,meter_combo.get())
    


    
def add_dept():
    def add_dept_data():
        departments=apitest.get_deptnames()
        dname=deptname_ent.get()
        hdname=headname_ent.get()
        emid=emailid_ent.get()
        paswd=password_ent.get()
        allvalues=[dname,hdname,emid,paswd]
        apitest.add_area(allvalues)
        dept_add.destroy()
    dept_add=Toplevel()
    deptname=Label(dept_add,text='Area Name',font=("helvatica",'16'),width=15)
    headname=Label(dept_add,text='Area Head',font=("helvatica",'16'),width=15)
    emailid=Label(dept_add,text='Email Id',font=("helvatica",'16'),width=15)
    password=Label(dept_add,text='Password',font=("helvatica",'16'),width=15)

    deptname.grid(row=0,column=0,padx=35,pady=5)
    headname.grid(row=1,column=0,padx=35,pady=5)
    emailid.grid(row=2,column=0,padx=35,pady=5)
    password.grid(row=3,column=0,padx=35,pady=5)

    deptname_ent=Entry(dept_add,width=35)
    headname_ent=Entry(dept_add,width=35)
    emailid_ent=Entry(dept_add,width=35)
    password_ent=Entry(dept_add,width=35)

    deptname_ent.grid(row=0,column=1,padx=35,pady=5)
    headname_ent.grid(row=1,column=1,padx=35,pady=5)
    emailid_ent.grid(row=2,column=1,padx=35,pady=5)
    password_ent.grid(row=3,column=1,padx=35,pady=5)

    submit_but=Button(dept_add,text='Submit',font=("helvatica",'14'),width=15,command=add_dept_data)
    submit_but.grid(row=4,column=0,padx=35,pady=5)
    canc_but=Button(dept_add,text='Cancel',font=("helvatica",'14'),width=15,command=dept_add.destroy)
    canc_but.grid(row=4,column=1,padx=35,pady=5)

def add_meter_data():
    global meters,meternames
    meter_values=[]
    meter_values.append(adm_block_combo.get())
    meter_values.append(adm_meter_name_ent.get())
    meter_values.append(adm_meter_slave_id_ent.get())
    apitest.add_meter(meter_values)
    tkMessageBox.showerror("Success","Meter Added Successfully")
    meters=apitest.get_metdept()
    meternames=apitest.get_meternames()
    frame_class.pack_one(2,[0,0])
    adm_block_combo.set('')
    adm_meter_name_ent.delete(0, END)
    adm_meter_slave_id_ent.delete(0, END)
    
def add_production_values():
    if cases_ent.get().strip()=='':
        pass
    else:
        apis.add_cases(cases_ent.get())
        cases_ent.delete(0, END)
    if chilled_ent.get().strip()=='':
        pass
    else:
        apis.add_chilled(chilled_ent.get())
        chilled_ent.delete(0, END)
    if unchilled_ent.get().strip()=='':
        pass
    else:
        apis.add_unchilled(unchilled_ent.get())
        unchilled_ent.delete(0, END)
    if manpower_ent.get().strip()=='':
        pass
    else:
        apis.add_manpower(manpower_ent.get())
        manpower_ent.delete(0, END)
    if dg1f_ent.get().strip()=='':
        pass
    else:
        apis.add_dg1fuel(dg1f_ent.get())
        dg1f_ent.delete(0, END)
    if dg2f_ent.get().strip()=='':
        pass
    else:
        apis.add_dg2fuel(dg2f_ent.get())
        dg2f_ent.delete(0, END)
        frame_class.pack_one(0,[0,0])
    acq.set_report()


##setting up the main root window    
root=Tk()


##make the screen full_screen
root.geometry("{0}x{1}".format(root.winfo_screenwidth(), root.winfo_screenheight()))


##header frame:this frame contains the header contents
head_frame=Frame(root,background='cyan',width=1500,height=100)
head_frame.grid(row=0,column=0,sticky='e')
head_frame.grid_propagate(0)


#calls pics to be added to the frotend
photo=PhotoImage(file="ems.gif")
photo1=PhotoImage(file="images.gif")


#main frame-contains main_body frame,border_frame,container for everything except header frame
main_frame=Frame(root,width=1500,height=800)
main_frame.grid(row=1,column=0)
main_frame.grid_propagate(0)


# toolbar frame:contains all the buttons add,edit,view,realtime etc
tool_frame=Frame(main_frame,background='cyan',width=300,height=800)
tool_frame.grid(row=0,column=0,sticky='w')
tool_frame.grid_propagate(0)


# main_body_frame :conatins main body and all the major frames
main_body_frame=Frame(main_frame,background='cyan',width=1200,height=800)
main_body_frame.grid(row=0,column=1,sticky='w')
main_body_frame.grid_propagate(0)


#add photo to the main body frame
lbl=Label(main_body_frame,image=photo)
lbl.place(x=0, y=0)


#border frame
border_frame=Frame(main_body_frame,background='grey',width=700,height=380)
border_frame.grid(row=0,column=0,padx=220,pady=100)
border_frame.grid_propagate(0)

#header content
header=Label(head_frame,text="Smart-EM",font=("helvatica",'58'),bg='cyan',fg='blue')
header.grid(row=0,column=0,sticky='e',padx='20')
frame_class=custom_wid.multi_frame(main_body_frame,photo)
frames=frame_class.create(18)

#view_frame_content
block_name=Label(frames[1],text='Select Area',font=("helvatica",'18'),width=15)
block_name.grid(row=0,column=0,padx=35,pady=30)
area_name=Label(frames[1],text='Select Meter',font=("helvatica",'18'),width=15)
area_name.grid(row=1,column=0,padx=35,pady=20)
block_combo=ttk.Combobox(frames[1],width=25)
block_combo.grid(row=0,column=1,padx=35,pady=20)
meter_combo=ttk.Combobox(frames[1],width=25)
meter_combo.grid(row=1,column=1,padx=35,pady=20)
block_combo.bind("<<ComboboxSelected>>",callback1)
home_but=Button(frames[1],text='Home',font=("helvatica",'18'),width=15,command=lambda:frame_class.pack_one(0,[0,0]))
home_but.grid(row=2,column=0,padx=35,pady=30)
home_but=Button(frames[1],text='Submit',font=("helvatica",'18'),width=15,command=create_new_frame)
home_but.grid(row=2,column=1,padx=35,pady=30)

#add_frame_contents
add_block=Button(frames[2],text='Add Area',font=("helvatica",'18'),width=15,command=add_dept)
add_block.grid(row=0,column=0,padx=35,pady=30)
add_meter=Button(frames[2],text='Add Meter',font=("helvatica",'18'),width=15,command=lambda:frame_class.pack_one(5,[0,0]))
add_meter.grid(row=0,column=1,padx=35,pady=30)
home_but1=Button(frames[2],text='Home',font=("helvatica",'18'),width=15,command=lambda:frame_class.pack_one(0,[0,0]))
home_but1.grid(row=1,column=0,padx=35,pady=30,columnspan=2)

#edit_frame_contents

cases_lbl=Label(frames[3],text='Cases Produced',font=("helvatica",'15'),width=15)
cases_lbl.grid(row=0,column=0,padx=50,pady=5)
chilled_lbl=Label(frames[3],text='Chilled Blend',font=("helvatica",'15'),width=15)
chilled_lbl.grid(row=1,column=0,padx=50,pady=5)
unchilled_lbl=Label(frames[3],text='Unchilled Blend',font=("helvatica",'15'),width=15)
unchilled_lbl.grid(row=2,column=0,padx=50,pady=5)
manpower=Label(frames[3],text='Manpower',font=("helvatica",'15'),width=15)
manpower.grid(row=3,column=0,padx=50,pady=5)
dg1f_lbl=Label(frames[3],text='DG1 Fuel',font=("helvatica",'15'),width=15)
dg1f_lbl.grid(row=4,column=0,padx=50,pady=5)
dg2f_lbl=Label(frames[3],text='DG2 Fuel',font=("helvatica",'15'),width=15)
dg2f_lbl.grid(row=5,column=0,padx=50,pady=5)
cases_ent=Entry(frames[3],width=25)
cases_ent.grid(row=0,column=1,padx=10,pady=5)
chilled_ent=Entry(frames[3],width=25)
chilled_ent.grid(row=1,column=1,padx=10,pady=5)
unchilled_ent=Entry(frames[3],width=25)
unchilled_ent.grid(row=2,column=1,padx=10,pady=5)
manpower_ent=Entry(frames[3],width=25)
manpower_ent.grid(row=3,column=1,padx=10,pady=5)
dg1f_ent=Entry(frames[3],width=25)
dg1f_ent.grid(row=4,column=1,padx=10,pady=5)
dg2f_ent=Entry(frames[3],width=25)
dg2f_ent.grid(row=5,column=1,padx=10,pady=5)
home_but2=Button(frames[3],text='Home',font=("helvatica",'16'),width=15,command=lambda:frame_class.pack_one(0,[0,0]))
home_but2.grid(row=6,column=0,padx=35,pady=5)
home_but2=Button(frames[3],text='Submit',font=("helvatica",'16'),width=15,command=add_production_values)
home_but2.grid(row=6,column=1,padx=35,pady=5)

#view_frame_contents
view_block=Button(frames[4],text='View Areas',font=("helvatica",'18'),width=15)
view_block.grid(row=0,column=0,padx=35,pady=30)
view_meter=Button(frames[4],text='View Meters',font=("helvatica",'18'),width=15,command=view_meter_details)
view_meter.grid(row=0,column=1,padx=35,pady=30)
home_but3=Button(frames[4],text='Home',font=("helvatica",'18'),width=15,command=lambda:frame_class.pack_one(0,[0,0]))
home_but3.grid(row=1,column=0,padx=35,pady=30,columnspan=2)

#add_meter_frame
adm_block_name=Label(frames[5],text='Select Area',font=("helvatica",'18'),width=15)
adm_block_name.grid(row=0,column=0,padx=35,pady=10)
adm_block_combo=ttk.Combobox(frames[5],values=departments,width=25)
adm_block_combo.grid(row=0,column=1,padx=35,pady=10)
adm_meter_name=Label(frames[5],text='Meter Name',font=("helvatica",'18'),width=15)
adm_meter_slave_id=Label(frames[5],text='Slave Id',font=("helvatica",'18'),width=15)
adm_meter_name_ent=Entry(frames[5],width=30)


adm_meter_slave_id_ent=Entry(frames[5],width=30)
adm_meter_name.grid(row=1,column=0,padx=35,pady=10)
adm_meter_name_ent.grid(row=1,column=1,padx=35,pady=10)
adm_meter_slave_id.grid(row=2,column=0,padx=35,pady=10)
adm_meter_slave_id_ent.grid(row=2,column=1,padx=35,pady=20)
adm_submit_but=Button(frames[5],text='Submit',font=("helvatica",'18'),width=15,command=add_meter_data)
adm_submit_but.grid(row=3,column=0,padx=35,pady=10)
adm_canc_but=Button(frames[5],text='Cancel',font=("helvatica",'18'),width=15,command=lambda:frame_class.pack_one(2,[0,0]))
adm_canc_but.grid(row=3,column=1,padx=35,pady=10)

#edit_area_Frame
edm_block_name=Label(frames[6],text='Select Area',font=("helvatica",'18'),width=15)
edm_block_name.grid(row=0,column=0,padx=35,pady=35)
edm_block_combo=ttk.Combobox(frames[6],values=departments,width=25)
edm_block_combo.grid(row=0,column=1,padx=35,pady=35)
edm_submit_but=Button(frames[6],text='Submit',font=("helvatica",'18'),width=15)
edm_submit_but.grid(row=1,column=0,padx=35,pady=35)
edm_canc_but=Button(frames[6],text='Cancel',font=("helvatica",'18'),width=15)
edm_canc_but.grid(row=1,column=1,padx=35,pady=35)

#edit_meter_Frame
edm_block_name1=Label(frames[7],text='Select Area',font=("helvatica",'18'),width=15)
edm_block_name1.grid(row=0,column=0,padx=35,pady=35)
edm_block_combo1=ttk.Combobox(frames[7],values=departments,width=25)
edm_block_combo1.grid(row=0,column=1,padx=35,pady=35)
edm_meter_name=Label(frames[7],text='Select Meter',font=("helvatica",'18'),width=15)
edm_meter_name.grid(row=1,column=0,padx=35,pady=35)
edm_meter_combo=ttk.Combobox(frames[7],width=25)
edm_meter_combo.grid(row=1,column=1,padx=35,pady=35)
edm_meter_combo.bind("<<ComboboxSelected>>",callback2)
edm1_submit_but=Button(frames[7],text='Submit',font=("helvatica",'18'),width=15)
edm1_submit_but.grid(row=2,column=0,padx=35,pady=35)
edm1_canc_but=Button(frames[7],text='Cancel',font=("helvatica",'18'),width=15)
edm1_canc_but.grid(row=2,column=1,padx=35,pady=35)

#meters
pass_block_name2=Button(frames[10],text='Change Password',width=20,font=("helvatica",'18'),command=lambda:frame_class.pack_one(9,[0,0]))
pass_block_name2.grid(row=0,column=0,padx=35,pady=15)
pass_block_name2=Button(frames[10],text='Change Report Time',width=20,font=("helvatica",'18'),command=lambda:frame_class.pack_one(11,[0,0]))
pass_block_name2.grid(row=1,column=0,padx=35,pady=15)

#change pasword
def change_pass():
    old_pass=old_pass_ent.get()
    new_pass=new_pass_ent.get()
    stored=apis.get_keys()[0][1]
    if old_pass==stored:
        apis.update_password(str(new_pass))
        old_pass_ent.delete(0,END)
        new_pass_ent.delete(0,END)
        frame_class.pack_one(0,[0,0])

    
def change_time():
    old_pass1=old_pass_ent1.get()
    stored=apis.get_keys()[0][1]
    if old_pass1==stored:
        values=['','']
        values[0]=new_time_hour_ent.get()
        values[1]=new_time_mint_ent.get()
        apis.update_time(values)
        old_pass_ent.delete(0,END)
        frame_class.pack_one(0,[0,0])
        
        
    
old_pass_block_name2=Label(frames[9],text='Old Password',font=("helvatica",'18'),width=15)
old_pass_block_name2.grid(row=0,column=0,padx=35,pady=35)
old_pass_ent=Entry(frames[9],width=25,show='*')
old_pass_ent.grid(row=0,column=1)
new_pass_block_name2=Label(frames[9],text='New Password',font=("helvatica",'18'),width=15)
new_pass_block_name2.grid(row=1,column=0,padx=35,pady=35)
new_pass_ent=Entry(frames[9],width=25,show='*')
new_pass_ent.grid(row=1,column=1)
pass_block_but2=Button(frames[9],text='Submit',width=15,font=("helvatica",'18'),command=change_pass)
pass_block_but2.grid(row=2,column=1,padx=35,pady=35)
pass_home_but=Button(frames[9],text='Home',width=15,font=("helvatica",'18'),command=lambda:frame_class.pack_one(0,[0,0]))
pass_home_but.grid(row=2,column=0,padx=35,pady=35)

hour_options=['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23']
mint_options=['05','10','15','20','25','30','35','40','45','50','55','00']


old_pass_block_name3=Label(frames[11],text='Old Password',font=("helvatica",'18'),width=15)
old_pass_block_name3.grid(row=0,column=0,padx=15,pady=25)
old_pass_ent1=Entry(frames[11],width=25,show='*')
old_pass_ent1.grid(row=0,column=1)
new_time_block_name2=Label(frames[11],text='New Time',font=("helvatica",'18'),width=15)
new_time_block_name2.grid(row=1,column=0,padx=15,pady=25)
new_time_hour_ent=ttk.Combobox(frames[11],width=10,values=hour_options)
new_time_hour_ent.grid(row=1,column=1)
new_time_mint_ent=ttk.Combobox(frames[11],width=10,values=mint_options)
new_time_mint_ent.grid(row=1,column=2)
new_time_hour_ent_v=Label(frames[11],width=10,text='Hour')
new_time_hour_ent_v.grid(row=2,column=1)
new_time_mint_ent_v=Label(frames[11],width=10,text='Minute')
new_time_mint_ent_v.grid(row=2,column=2)
pass_block_but3=Button(frames[11],text='Submit',width=15,font=("helvatica",'18'),command=change_time)
pass_block_but3.grid(row=3,column=1,padx=15,pady=25)
pass_home_but1=Button(frames[11],text='Home',width=15,font=("helvatica",'18'),command=lambda:frame_class.pack_one(0,[0,0]))
pass_home_but1.grid(row=3,column=0,padx=15,pady=25)


edm_meter_combo2=Label(frames[8],text='MDI',font=("helvatica",'18'),width=15)
edm_meter_combo2.grid(row=0,column=0,padx=35,pady=35)
edm_meter_ent2=Label(frames[8],font=("helvatica",'18'),width=15)
edm_meter_ent2.grid(row=0,column=1)


def mid_frame_pack():
    frame_class.pack_one(8,[0,0])
    edm_meter_ent2.config(text=str(acq.get_mdi()))
    
def set_thr():
    def hh():
        values=[]
        for i in range(len(ents)):
            val=ents[i].get()
            if val.strip()=='':
                pass
            else:
                apis.add_thresholds(i+1,val)
    topq=Toplevel()
    topq.geometry('400x1000')
    lbls=[]
    ents=[]
    mids=apis.get_all_m_id()
    for i in range(len(mids)):
        lbls.append(Label(topq,text=str(apis.mname_from_mid(mids[i])),font=("helvatica",'10'),width=20))
        lbls[i].grid(row=i,column=0)
        ents.append(Entry(topq,width=20))
        ents[i].grid(row=i,column=1)
    but=Button(topq,text='Submit',font=("helvatica",'10'),width=20,command=hh)
    but.grid(row=i+1,column=0,columnspan=2)

def check_thr():
    def check():
        keys=apis.get_keys()
        if str(ent.get())==str(keys[0][1]):
            set_thr()
            top.destroy()
        
    top=Toplevel()
    lbl=Label(top,text='Password').grid(row=0,column=0)
    ent=Entry(top,show='*')
    ent.grid(row=0,column=1)
    but=Button(top,text='OK',command=check)
    but.grid(row=1,column=0,columnspan=2)

    
    
def add_production():
    def check():
        keys=apis.get_keys()
        if str(ent.get())==str(keys[0][1]):
            frame_class.pack_one(3,[0,0])
            top.destroy()
        
    top=Toplevel()
    lbl=Label(top,text='Password').grid(row=0,column=0)
    ent=Entry(top,show='*')
    ent.grid(row=0,column=1)
    but=Button(top,text='OK',command=check)
    but.grid(row=1,column=0,columnspan=2)

def check_report():
    def check():
        keys=apis.get_keys()
        if str(ent.get())==str(keys[0][1]):
            acq.report_daily_thread()
            top.destroy()
        
    top=Toplevel()
    lbl=Label(top,text='Password').grid(row=0,column=0)
    ent=Entry(top,show='*')
    ent.grid(row=0,column=1)
    but=Button(top,text='OK',command=check)
    but.grid(row=1,column=0,columnspan=2)
    
        
def check_setting():
    def check():
        keys=apis.get_keys()
        if str(ent.get())==str(keys[0][1]):
            frame_class.pack_one(10,[0,0])
            top.destroy()
        
    top=Toplevel()
    lbl=Label(top,text='Password').grid(row=0,column=0)
    ent=Entry(top,show='*')
    ent.grid(row=0,column=1)
    but=Button(top,text='OK',command=check)
    but.grid(row=1,column=0,columnspan=2)        
    
#toolbar settings
add_button=Button(tool_frame,text="Real time",font=("helvatica",'18'),width=22,fg='green',command=parameter_frame)
view_button=Button(tool_frame,text='MDI',font=("helvatica",'18'),width=22,fg='green',command=mid_frame_pack)
edit_button=Button(tool_frame,text='Production',font=("helvatica",'18'),width=22,fg='green',command=add_production)
delete_button=Button(tool_frame,text='Generate Report',font=("helvatica",'18'),width=22,fg='green',command=check_report)
thr_button=Button(tool_frame,text='Set Threshold',font=("helvatica",'18'),width=22,fg='green',command=check_thr)
setting_button=Button(tool_frame,text='Setting',font=("helvatica",'18'),width=22,fg='green',command=check_setting)
add_button.grid(row=0,column=0,pady=5)
view_button.grid(row=1,column=0,pady=5)
edit_button.grid(row=2,column=0,pady=5)
delete_button.grid(row=3,column=0,pady=5)
frame_class.pack_one(0,[0,0])
setting_button.grid(row=5,column=0,pady=5)
thr_button.grid(row=4,column=0,pady=5)
#get()
#print('hete')
root.mainloop()
