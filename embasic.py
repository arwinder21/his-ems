from tkinter import *
import serial
import serial.tools.list_ports as list_ports
import minimalmodbus
import json
from time import sleep
import urllib2
import os.path,winshell
from win32com.client import Dispatch
import requests
import socket
import datetime
comlist = serial.tools.list_ports.comports()
connected = []
import ctypes, sys

def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False

if is_admin():
    # Code of your program here
    from win32com.shell import shell, shellcon
    import subprocess
    import os

    import win32com.shell.shell as shell
    import winshell, admin


    def startupdirectory():

        path = shell.SHGetFolderPath(
            0,
            shellcon.CSIDL_COMMON_STARTUP,
            0,  # null access token (no impersonation)
            0  # want current value, shellcon.SHGFP_TYPE_CURRENT isn't available, this seems to work
        )
        shortcut = winshell.shortcut(os.path.realpath(__file__))
        shortcut.working_directory = os.path.realpath(__file__)
        shortcut.write(os.path.join(path, "embasic.lnk"))
        shortcut.dump()


    startupdirectory()
else:
    # Re-run the program with admin rights
    ctypes.windll.shell32.ShellExecuteW(None, u"runas", unicode(sys.executable), unicode(__file__), None, 1)
for element in comlist:
    connected.append(element.device)
#file_path="C:\Users\Arwinder\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\Dropbox"
#print(os.path.islink(file_path))
#print(os.stat(file_path))
#print(connected)
#print(os.path.realpath(__file__))
def is_connected():
    try:
        # connect to the host -- tells us if the host is actually
        # reachable
        socket.create_connection(("www.google.com", 80))
        return True
    except OSError:
        pass
    return False
print(is_connected())
#comport=raw_input("Enter the comport")
slaveid=1
def initiate(connected,slaveid,i):
    if (i<30):
        try:
            instrument=minimalmodbus.Instrument(connected[0], slaveid)
            instrument.serial.baudrate = 9600
            instrument.serial.bytesize = 8
            instrument.serial.parity = serial.PARITY_NONE
            instrument.serial.stopbits = 1
            instrument.serial.timeout = 200
            instrument.serial.close_after_each_call = TRUE
            print('Initialization sucessful')
            return(instrument)
        except:
            i=i+1
            print('Initialization Failed,Trying again')
            initiate(connected[0],slaveid,i)
ins=initiate(connected,slaveid,0)
param_ids=["1","2","3","4","5","6","7"]
param_names=["Temprature1","Temprature2","Flow","KW(H)","KWHr(H)","KW(C)","KWHr(C)"]

while(1):
    data_temp=[]
    data_temp.append(ins.read_float(1,3,2))
    data_temp.append(ins.read_float(3,3,2))
    data_temp.append(ins.read_float(5,3,2))
    data_temp.append(ins.read_float(7,3,2))
    data_temp.append(ins.read_float(11,3,2))
    data_temp.append(ins.read_float(13,3,2))
    data_temp.append(ins.read_float(15,3,2))
    data_temp.append(ins.read_float(17,3,2))
    data_temp.append(ins.read_float(19,3,2))
    data_temp.append(ins.read_float(21,3,2))
    data_temp.append(ins.read_float(23,3,2))
    data_temp.append(ins.read_float(25,3,2))
    data_temp.append(ins.read_float(27,3,2))
    data_temp.append(ins.read_float(29,3,2))
    data_temp.append(ins.read_float(31,3,2))
    data_temp.append(ins.read_float(33,3,2))
    data_temp.append(ins.read_float(35,3,2))
    data_temp.append(ins.read_float(37,3,2))
    data_temp.append(ins.read_float(39,3,2))
    data_temp.append(ins.read_float(41,3,2))
    data_temp.append(ins.read_float(43,3,2))
    data_temp.append(ins.read_float(45,3,2))
    data_temp.append(ins.read_float(47,3,2))
    data_temp.append(ins.read_float(49,3,2))
    data_temp.append(ins.read_float(51,3,2))
    data_temp.append(ins.read_float(53,3,2))
    data_temp.append(ins.read_float(55,3,2))
    data_temp.append(ins.read_float(57,3,2))
    data_temp.append(ins.read_float(59,3,2))
    data_temp.append(ins.read_float(61,3,2))
    data_temp.append(ins.read_float(63,3,2))
    for i in range(0,len(data_temp)):
        data=data_temp[i]*1000
        data=int(data)
        data=float(data)
        data_temp[i]=data/1000
    timestamp=str(datetime.datetime.now())
    print(data_temp)

    url = 'http://industrylog.org/emsapi/REST/loggings/'
    payload = [{"param_id": param_ids[0], "param_value": str(data_temp[0]), "timestamp1": timestamp},{"param_id": param_ids[1], "param_value": str(data_temp[1]),"timestamp1": timestamp},{"param_id": param_ids[2], "param_value": str(data_temp[2]),"timestamp1": timestamp},\
               {"param_id": param_ids[3], "param_value": str(data_temp[3]),"timestamp1": timestamp},{"param_id": param_ids[4], "param_value": str(data_temp[4]),"timestamp1": timestamp}]
    print(payload[0])
    for i in range(0,len(payload)):
        r = requests.post(url, json=(payload[i]))
        
    print(data_temp)
    sleep(40)
