import minimalmodbus
import serial
import serial.tools.list_ports as list_ports
import time
import threading
import apis
import time,datetime
import schedule


rl = threading.RLock()
a=apis.get_keys()
timevalue=str(a[0][2])+':'+str(a[0][3])
comlist = serial.tools.list_ports.comports()
connected = []

for element in comlist:
    connected.append(element.device)

print(connected)
timenow=datetime.datetime.now()
timenow=str(timenow)
fileopen=open("tcl\logs.txt","a+")
fileopen.write("started at :"+timenow+" comm selected:"+str(connected[0])+"\n")
fileopen.close()
def initiate(connected,slaveid):
    #initiates the code
    inst=minimalmodbus.Instrument(connected, slaveid)
    inst.serial.baudrate = 9600
    inst.serial.bytesize = 8
    inst.serial.parity=serial.PARITY_NONE
    inst.serial.stopbits = 1
    inst.serial.timeout = 5
    inst.serial.close_after_each_call = True
    inst.handle_local_ecgo = True
##    inst.baudrate = 9600
##    inst.bytesize = 8
##    inst.parity=serial.PARITY_NONE
##    inst.stopbits = 1
##    inst.timeout = 5
##    inst.close_after_each_call = True
    return(inst)
meters=[]

def initiate_all(num,slave_id,connected):
    global meters
    for i in range(0,num):
        meters.append(initiate(connected,slave_id[i]))
    return(meters)

def read_float_all(meter):
    values=[]
    i=0
    while(i<63):
        #time.sleep(0.1)
        try:
            #print(values)
            values.append(meter.read_float(i,4,2,3))
            i+=2
        except:
            values.append('error')        
    return(values)

def read_kwh(meter,s_id):
    global all_values
    values=all_values[str(s_id)][29]
    return(values)

def read_kva(meter):
    try:
        values=(meter.read_float(44,4,2,3))
    except:
        values=0
    return(values)

def read_kvh(meter):
    try:
        values=(meter.read_float(60,4,2,3))
    except:
        values=0
    return(values)

def read_pf(meter):
    try:
        values=(meter.read_float(54,4,2,3))

    except:
        values=0
    return(values)


slave_id=apis.get_all_slave_id()
print(slave_id)
c=initiate_all(26,slave_id,connected[0])
#c=initiate_all(26,slave_id,'connected')
all_values={}
all_threads=[]
value_present=False

for i in slave_id:
    all_values[str(i)]='NULL'

def add_start_kwh():
    meter_id=1
    print('add_start_kwh')
    while meter_id<29:
        s_id=apis.slave_id_from_mid(meter_id)
        meter_obj=c[s_id-1]
        #try:
        time.sleep(1)
        energy=(read_kwh(meter_obj,s_id))
        print(energy)
        apis.add_start_values(meter_id,energy)
        meter_id+=1
        print('value added of meter id:'+str(meter_id-1))
##        except:
##            print('retrying meter id:'+str(meter_id))
##            time.sleep(1)
##            add_start_kwh()
            
            

def add_start_kwh_daily():
    meter_id=1
    while meter_id<29:
        s_id=apis.slave_id_from_mid(meter_id)
        meter_obj=c[s_id-1]
        try:
            time.sleep(1)
            energy=(read_kwh(meter_obj,s_id))
            print(energy)
            apis.add_start_values_daily(meter_id,energy)
            meter_id+=1
            print('value added of meter id:'+str(meter_id-1))
        except:
            print('retrying meter id:'+str(meter_id))
            time.sleep(1)
            add_start_kwh_daily()
           
 
            



def add_end_kwh():
    meter_id=1
    while meter_id<29:
        s_id=apis.slave_id_from_mid(meter_id)
        meter_obj=c[s_id-1]
        try:
            time.sleep(1)
            energy=(read_kwh(meter_obj,s_id))
            apis.add_end_values(meter_id,energy)
            meter_id+=1
            print(energy)
            print('value added of meter id:'+str(meter_id-1))
        except:
            print('retrying meter id:'+str(meter_id))
            time.sleep(1)
            add_end_kwh()
            

def add_end_kwh_daily():
    meter_id=1
    while meter_id<29:
        s_id=apis.slave_id_from_mid(meter_id)
        meter_obj=c[s_id-1]
        
        time.sleep(1)
        energy=(read_kwh(meter_obj,s_id))
        apis.add_end_values_daily(meter_id,energy)
        meter_id+=1
        print(energy)
        print('value added of meter id:'+str(meter_id-1))
        
##            print('retrying meter id:'+str(meter_id))
##            time.sleep(1)
##            add_start_kwh()

def report_daily():
    print('generating daily report')
    add_end_kwh_daily()
    print('end_values_added')
    apis.generate_daily_kwh_rpt()
    add_start_kwh_daily()
    datenow=datetime.datetime.now()
    date=datenow.day
    if date==1:
        report_monthly()
    else:
        print("same_month")

def report_daily_thread():
    thread_repd=threading.Thread(target=apis.generate_manual_report)
    thread_repd.start()


def report_daily_kpa():
    print("Generating daily kpa report")
    add_end_kwh()
    print("end values added")
    apis.generate_report_daily()
    add_start_kwh()
    


def report_monthly():
    apis.generate_report_monthly()
    

def get_values():
    global c,all_values,slave_id,value_present
    while(1):
        for i in range(len(slave_id)):
            #print(all_values)
            #print(i)
            #try:
                all_values[str(slave_id[i])]=read_float_all(c[slave_id[i]-1])
                print(all_values)
            #except:
            #    print('exception in getting values')

        

def get_value_once():
    global all_values
    print('getting_values')
    for i in range(len(slave_id)):
        try:
            a=read_float_all(c[slave_id[i]-1])
            #print(a)
            if 'error' in a:
                time.sleep(10)
                a=read_float_all(c[slave_id[i]-1])
            else:
                pass
            all_values[str(slave_id[i])]=a
        except:
            print('exception in getting values')

        
        

def fetch_values():
    global all_values
    
    return(all_values)
            #print(all_values)



def thread_job():
    global sync_var,reporting
    
    while 1:
        if sync_var==True:
        #rl.acquire()
            print("inside the schedule loop")
            schedule.run_pending()
            time.sleep(60)
            if reporting==True:
                report_daily_kpa()
                reporting=False
            sync_var=False
        #rl.release()

print(timevalue)

schedule.every().day.at(timevalue).do(report_daily)
mdi=0
reporting=False

def set_report():
    global reporting
    reporting=True

def set_report_kpi():
    global reporting
    reporting=2
    print(reporting)

sync_var = False
def main_func():
    global all_values,mdi,reporting,sync_var
    get_value_once()  #update all the values of parameters
    add_start_kwh()   #add start kwh values
    add_start_kwh_daily()
    #add start kwh values 
    date_now=datetime.datetime.now().day    # get current date 
    month_now=datetime.datetime.now().month  # get current month
    while (1):
        #rl.acquire()
        if sync_var==False:
            total_sum=0        #sum variable of mdi
            pf_sum=0         
            pf_count=0
            count=0
            m_name='Mains'      
            slave=apis.slave_id_from_mname(m_name)
            meter_obj=c[slave-1]
            start_time=time.time()
            print("entering mdi loop")
            print(str(datetime.datetime.now()))
            while(1):
                now_time=time.time()
                elapsed=int(now_time)-int(start_time)
                if elapsed<300:
                    try:
                        kva=read_kva(meter_obj)
                        pf=read_pf(meter_obj)
                        pf_sum+=abs(pf)
                        pf_count+=1
                        total_sum+=float(abs(kva))
                        count+=1
                    except:
                        pass
                else:
                    print("out of mdi loop")
                    print(str(datetime.datetime.now()))
                    mdi_temp=total_sum/count
                    get_value_once()
                    dg_values=apis.get_dg_values()
                    print("getting dg values")
                    dg1=all_values['1']
                    print("dg1 acquired")
                    dg2=all_values['21']
                    print("dg2 acquired")
                    try:
                        if int(dg1[11])>1:
                            dg1rh=float(dg_values[0])
                            dg1rh+=(0.84)
                            apis.add_dg1rh(dg1rh)
                    except:
                        pass
                    try:
                        if int(dg2[11])>1:
                            dg2rh=float(dg_values[1])
                            dg2rh+=(0.84)
                            apis.add_dg2rh(dg2rh)
                    except:
                        
                        pass
                    countml=0
                    while(countml<28):
                        ss=apis.slave_id_from_mid(str(countml+1))
                        try:
                            apis.insert_values_meter(countml,(all_values[str(ss)][0:30]+[mdi_temp]))
                            countml+=1
                        except:
                            time.sleep(5)
                    try:
                        apis.add_pf_values(pf,pf_count)
                    except:
                        pass
                    if mdi_temp>mdi:
                        mdi=mdi_temp
                        try:
                            apis.add_mdi(mdi)
                        except:
                            pass
                    else:
                        pass
            #rl.release()
                    print("breaking out loop")
                    sync_var=True
                    print("sync set to true")
                    time.sleep(5)
                    break



                    
def get_mdi():
    global mdi
    mdi=float(int(float(apis.get_mdi())*1000))/1000
    return(mdi)

#report_daily()
##report_daily()
all_threads=(threading.Thread(target=main_func))
all_threads.start()
all_thread1=(threading.Thread(target=thread_job))
all_thread1.start()
#report()


##report_daily()
