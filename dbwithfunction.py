import sqlite3

def create_table():

        conn=sqlite3.connect('databs.db')
        conn.execute('''create table if not exists Parameters
                        (Date_Time                char          not null,
                        Parameter_ID            char          not null,
                        Parameter_Name      char          not null,
                        parameter_Values      char        not null)''')
        print ("function is created")
        conn.commit()
        conn.close()

def ins_value(data):
        conn=sqlite3.connect('databs.db')
        conn.execute("insert into Parameters  values (?,?,?,?)",data)

        conn.commit()
        conn.close()

create_table()
