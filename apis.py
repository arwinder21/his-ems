#######IMPORTS########################################################################
#import urllib2
#import requests
#import json
#import socket
import sqlite3
import time, datetime
##from openpyxl import Workbook
import xlsxwriter
#######CREATE MAIN DB########################################################################
def create():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        create()
    cur = connect.cursor()
    first = '''CREATE TABLE IF NOT EXISTS  departments
         (
         dept_name       char      NOT NULL,
         head_name      char      NOT NULL,
         meters     integer     NOT NULL,
         heademail        char        NOT NULL

         )'''
    cur.execute(first)
    second = '''CREATE TABLE IF NOT EXISTS  meters
         (
         meter_name       char      NOT NULL,
         dept_name      char      NOT NULL,
         meter_id     integer     NOT NULL     Primary Key,
         slave_id     integer     NULL
         )'''
    cur.execute(second)
    third = '''CREATE TABLE IF NOT EXISTS  devices
         (
         slave_id       integer   Primary Key NOT NULL
         )'''
    cur.execute(third)
    fourth= '''CREATE TABLE IF NOT EXISTS  kwhreport
         (
         date             char       NOT NULL,
         meter_id       integer      NOT NULL,
         start_unitkwh     char     NOT NULL,
         end_unitkwh     char     NULL,
         consumedkwh      char    NULL
         )'''
    cur.execute(fourth)
    fifth= '''CREATE TABLE IF NOT EXISTS  other_report
         (
         pf_sum     char        NULL,
         pf_count     integer     NULL,
         cases        integer      NULL,
         chilled_blend   integer     NULL,
         unchilled_blend    integer    NULL,
         manpower        integer       NULL,
         mdi        char       NULL,
         dg1rh   char        NULL,
         dg2rh    char       NULL,
         dg1fuel   char      NULL,
         dg2fuel     char    NULL
         )'''
    cur.execute(fifth)
    sixth= '''CREATE TABLE IF NOT EXISTS  consumed_kwh
         (
         Date    char        NOT NULL,
         [1]    char        NOT NULL,
         [2]    char        NOT NULL,
         [3]    char        NOT NULL,
         [4]    char        NOT NULL,
         [5]    char        NOT NULL,
         [6]    char        NOT NULL,
         [7]    char        NOT NULL,
         [8]    char        NOT NULL,
         [9]    char        NOT NULL,
         [10]    char        NOT NULL,
         [11]    char        NOT NULL,
         [12]   char        NOT NULL,
         [13]    char        NOT NULL,
         [14]    char        NOT NULL,
         [15]    char        NOT NULL,
         [16]    char        NOT NULL,
         [17]    char        NOT NULL,
         [18]    char        NOT NULL,
         [19]    char        NOT NULL,
         [20]    char        NOT NULL,
         [21]    char        NOT NULL,
         [22]    char        NOT NULL,
         [23]    char        NOT NULL,
         [24]    char        NOT NULL,
         [25]    char        NOT NULL,
         [26]   char        NOT NULL,
         [27]    char        NOT NULL,
         [28]    char        NOT NULL
         
         )'''
    cur.execute(sixth)

    eighth= '''CREATE TABLE IF NOT EXISTS  overall
         (
         Date    char        NOT NULL,
         total_units    char        NOT NULL,
         pf_overall    char        NOT NULL,
         solar_units    char        NOT NULL,
         cases_produced    char        NOT NULL,
         chilled    char        NOT NULL,
         unchilled    char        NOT NULL,
         manpower    char        NOT NULL,
         units_case    char        NOT NULL,
         unit_chilled    char        NOT NULL,
         unit_unchilled    char        NOT NULL,
         unit_manpower    char        NOT NULL,
         mdi    char        NOT NULL,
         dg1rh    char        NOT NULL,
         dg2rh      char        NOT NULL,
         dh1fuel       char        NOT NULL,
         dg2fuel       char        NOT NULL,
         dg1eff         char        NOT NULL,
         dg2eff        char        NOT NULL,
         dg1units      char        NOT NULL,
         dg2units      char        NOT NULL

         )'''
    cur.execute(eighth)
    ninth= '''CREATE TABLE IF NOT EXISTS  threshholds
         (
         m1    char        NOT NULL,
         m2    char        NOT NULL,
         m3    char        NOT NULL,
         m4    char        NOT NULL,
         m5    char        NOT NULL,
         m6    char        NOT NULL,
         m7    char        NOT NULL,
         m8    char        NOT NULL,
         m9    char        NOT NULL,
         m10    char        NOT NULL,
         m11    char        NOT NULL,
         m12   char        NOT NULL,
         m13    char        NOT NULL,
         m14    char        NOT NULL,
         m15    char        NOT NULL,
         m16    char        NOT NULL,
         m17    char        NOT NULL,
         m18    char        NOT NULL,
         m19    char        NOT NULL,
         m20    char        NOT NULL,
         m21    char        NOT NULL,
         m22    char        NOT NULL,
         m23    char        NOT NULL,
         m24    char        NOT NULL,
         m25    char        NOT NULL,
         m26   char        NOT NULL,
         m27    char        NOT NULL,
         m28    char        NOT NULL

         )'''
    cur.execute(ninth)
    values=[]
    for row in cur.execute("SELECT m"+str(1)+ " from threshholds"):
            values.append(str(row[0]))
    #print(values)
    if values==[]:
        print('in insert')
        ff = '''INSERT INTO threshholds values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
        cur.execute(ff, ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28'))

    tenth= '''CREATE TABLE IF NOT EXISTS  rated_against
         (
         Date   char       NOT NULL,
         m1    char        NOT NULL,
         m2    char        NOT NULL,
         m3    char        NOT NULL,
         m4    char        NOT NULL,
         m5    char        NOT NULL,
         m6    char        NOT NULL,
         m7    char        NOT NULL,
         m8    char        NOT NULL,
         m9    char        NOT NULL,
         m10    char        NOT NULL,
         m11    char        NOT NULL,
         m12   char        NOT NULL,
         m13    char        NOT NULL,
         m14    char        NOT NULL,
         m15    char        NOT NULL,
         m16    char        NOT NULL,
         m17    char        NOT NULL,
         m18    char        NOT NULL,
         m19    char        NOT NULL,
         m20    char        NOT NULL,
         m21    char        NOT NULL,
         m22    char        NOT NULL,
         m23    char        NOT NULL,
         m24    char        NOT NULL,
         m25    char        NOT NULL,
         m26   char        NOT NULL,
         m27    char        NOT NULL,
         m28    char        NOT NULL

         )'''
    cur.execute(tenth)
    query= '''CREATE TABLE IF NOT EXISTS  misc
         (
            password    char        NOT NULL,
            key    char        NOT NULL,
            time_hour    char     NOT NULL,
            time_mint    char     NOT NULL

         )'''
    cur.execute(query)
    query= '''CREATE TABLE IF NOT EXISTS  daily_consumed_kwh
         (
         Date    char        NOT NULL,
         [1]    char        NOT NULL,
         [2]    char        NOT NULL,
         [3]    char        NOT NULL,
         [4]    char        NOT NULL,
         [5]    char        NOT NULL,
         [6]    char        NOT NULL,
         [7]    char        NOT NULL,
         [8]    char        NOT NULL,
         [9]    char        NOT NULL,
         [10]    char        NOT NULL,
         [11]    char        NOT NULL,
         [12]   char        NOT NULL,
         [13]    char        NOT NULL,
         [14]    char        NOT NULL,
         [15]    char        NOT NULL,
         [16]    char        NOT NULL,
         [17]    char        NOT NULL,
         [18]    char        NOT NULL,
         [19]    char        NOT NULL,
         [20]    char        NOT NULL,
         [21]    char        NOT NULL,
         [22]    char        NOT NULL,
         [23]    char        NOT NULL,
         [24]    char        NOT NULL,
         [25]    char        NOT NULL,
         [26]   char        NOT NULL,
         [27]    char        NOT NULL,
         [28]    char        NOT NULL
         
         )'''
    cur.execute(query)
    fourth= '''CREATE TABLE IF NOT EXISTS  kwhreport_daily
         (
         date             char       NOT NULL,
         meter_id       integer      NOT NULL,
         start_unitkwh     char     NOT NULL,
         end_unitkwh     char     NULL,
         consumedkwh      char    NULL
         )'''
    cur.execute(fourth)
    connect.commit()
    connect.close()
#######CREATE NEW DB TABLES############################################################
create()
#######GET KEY FROM DB#################################################################
def get_keys():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_keys()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT * FROM misc"):
        values.append((row))
    connect.close()
    return(values)

#######FUNCTION TO UPDATE PASSWORD IN DB################################################
def update_password(value):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        update_password(value)
    cur = connect.cursor()
    first = '''UPDATE misc SET key=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()

def update_time(value):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        update_time(value)
    cur = connect.cursor()
    first = '''UPDATE misc SET time_hour=?'''
    second = '''UPDATE misc SET time_mint=?'''
    cur.execute(first, (value[0],))
    cur.execute(second, (value[1],))
    connect.commit()
    connect.close()

    
######CHANGE PASSWORD AND KEYS BOTH #####################################################
def insert_keys(value):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        insert_keys(value)
    cur = connect.cursor()
    first = '''INSERT INTO misc VALUES (?,?,?,?)'''
    cur.execute(first, value)
    connect.commit()
    connect.close()
#######DROP MISC TABLE###################################################################
def dropkeys():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        dropkeys()
    cur = connect.cursor()
    cur.execute('''DROP TABLE IF EXISTS  misc''')
    connect.commit()
    connect.close()
#######IMPORTS########################################################################
def get_start_values(mid):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_start_values(mid)
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT start_unitkwh FROM kwhreport WHERE meter_id="+str(mid)):
        values.append(str(row[0]))
    value1=[]
##    for row in cur.execute("SELECT start_unitkvah FROM kwhreport WHERE meter_id="+str(mid)):
##        value1.append(str(row[0]))
    connect.close()
    try:
        return([values[0],value1[0]])
    except:
        return([values,value1])

    
def get_start_values_daily(mid):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_start_values_daily(mid)
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT start_unitkwh FROM kwhreport_daily WHERE meter_id="+str(mid)):
        values.append(str(row[0]))
    value1=[]
##    for row in cur.execute("SELECT start_unitkvah FROM kwhreport WHERE meter_id="+str(mid)):
##        value1.append(str(row[0]))
    connect.close()
    try:
        return([values[0],value1[0]])
    except:
        return([values,value1])

def normalize(val):
    try:
        return(float(int(float(val)*1000))/1000)
    except:
        return(val)
    
def add_start_values(mid,kwh):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_start_values(mid,kwh)
    cur = connect.cursor()
    vals=get_start_values(mid)
    if vals[0]==[]:
        date_now=datetime.datetime.now().date()
        first = '''INSERT INTO kwhreport (
                     date,
                     meter_id,
                     start_unitkwh
                       )
                    VALUES
                     (?,?,?)'''
        cur.execute(first, (date_now,normalize(mid),kwh,))
    connect.commit()
    connect.close()

def add_start_values_daily(mid,kwh):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_start_values_daily(mid,kwh)
    cur = connect.cursor()
    vals=get_start_values_daily(mid)
    if vals[0]==[]:
        date_now=datetime.datetime.now().date()
        first = '''INSERT INTO kwhreport_daily (
                     date,
                     meter_id,
                     start_unitkwh
                       )
                    VALUES
                     (?,?,?)'''
        cur.execute(first, (date_now,mid,kwh,))
    connect.commit()
    connect.close()

    
def set_default_values():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        set_default_values()
    cur = connect.cursor()
    for i in range(1,29):
        values=[]

        if values==[]:
            first = '''INSERT INTO threshholds ('''+str([i])+''') VALUES (?)'''
            cur.execute(first, ('0',))
        connect.commit()
        connect.close()
#set_default_values()
def add_thresholds(mid,kwh):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_thresholds(mid,kwh)
    cur = connect.cursor()
    first = '''UPDATE threshholds set m'''+str(mid)+'''=?'''
    cur.execute(first, (kwh,))
    connect.commit()
    connect.close()
def get_all_thresholds():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_all_thresholds()
    cur = connect.cursor()
    values=[]
    for i in range(1,29):
        for row in cur.execute("SELECT m"+str(i)+ " from threshholds"):
                values.append(str(row[0]))
    connect.close()
    return(values)
def add_all_rated(values):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_all_rated(values)
    cur = connect.cursor()
    ff = '''INSERT INTO rated_against values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cur.execute(ff, values)
    connect.commit()
    connect.close()
    return(values)

def add_end_values(mid,kwh):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_end_values(mid,kwh)
    cur = connect.cursor()
    first = '''UPDATE kwhreport set end_unitkwh=? where meter_id=?'''
    cur.execute(first, (normalize(kwh),mid,))
    connect.commit()
    connect.close()
    
    starts=get_start_values(mid)
    print(starts)
    print(kwh)
    connect = sqlite3.connect('data.db')
    cur = connect.cursor()
    ckwh=float(int((float(kwh)-float(starts[0][0]))*10))/10
    print(ckwh)
    #ckvah=float(int((float(kvh)-float(starts[1]))*10))/10
    if ckwh<0.1:
        ckwh=0

    first = '''UPDATE kwhreport set consumedkwh=? where meter_id=?'''
    cur.execute(first, (ckwh,mid,))
    connect.commit()
    connect.close()

def add_end_values_daily(mid,kwh):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_end_values_daily(mid,kwh)
    cur = connect.cursor()
    first = '''UPDATE kwhreport_daily set end_unitkwh=? where meter_id=?'''
    cur.execute(first, (normalize(kwh),mid,))
    connect.commit()
    connect.close()
    
    starts=get_start_values_daily(mid)
    print(starts)
    print(kwh)
    connect = sqlite3.connect('data.db')
    cur = connect.cursor()
    ckwh=float(int((float(kwh)-float(starts[0][0]))*10))/10
    #ckvah=float(int((float(kvh)-float(starts[1]))*10))/10
    if ckwh<0.1:
        ckwh=0
    print('ckwh'+str(mid)+str(ckwh))
    first = '''UPDATE kwhreport_daily set consumedkwh=? where meter_id=?'''
    cur.execute(first, (ckwh,mid,))
    connect.commit()
    connect.close()

def get_units_meter(mid):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_units_meter(mid)
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT consumedkwh FROM kwhreport WHERE meter_id="+str(mid)):
        values.append(str(row[0]))
    connect.commit()
    connect.close()

    return(values[0])

def get_kvah_meter(mid):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_kvah_meter(mid)
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT consumedkvah FROM kwhreport WHERE meter_id="+str(mid)):
        values.append(str(row[0]))
    connect.commit()
    connect.close()

    return(values[0])

def get_total_kwhr():
    solar=get_units_meter(19)
    dg1=get_units_meter(20)
    dg2=get_units_meter(21)
    mains=get_units_meter(22)
    total=float(solar)+float(dg1)+float(dg2)+float(mains)
    return(total,solar,mains,dg1,dg2)


def get_existing_PF_sum():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_existing_PF_sum()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT pf_sum FROM other_report"):
        values.append(str(row[0]))
    for row in cur.execute("SELECT pf_count FROM other_report"):
        values.append(str(row[0]))
    if values==[]:
        first = '''INSERT INTO other_report (
                     pf_sum,
                     pf_count
                       )
                    VALUES
                     (?,?,)'''
        cur.execute(first, (0,0))
        connect.commit()
        connect.close()
        return('0','0')
    connect.close()
    return(values)


def get_dg_values():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_dg_values()
    cur = connect.cursor()
    values=[]
    return_vals=[]
    for row in cur.execute("SELECT dg1rh FROM other_report"):
        values.append(row[0])
    print(values)
    if values==[]:
        first = '''INSERT INTO other_report (
                     dg1rh
                       )
                    VALUES
                     (?)'''
        cur.execute(first, ('0',))
        return_vals.append('0')
    else:
        return_vals.append(values[0])
    values=[]
    for row in cur.execute("SELECT dg2rh FROM other_report"):
        values.append(row[0])
    if values==[]:
        first = '''INSERT INTO other_report (
                     dg2rh
                       )
                    VALUES
                     (?)'''
        cur.execute(first, (0,))
        return_vals.append('0')
    else:
        return_vals.append(values[0])
    values=[]
    for row in cur.execute("SELECT dg1fuel FROM other_report"):
        values.append(row[0])
    if values==[]:
        first = '''INSERT INTO other_report (
                     dg1fuel
                       )
                    VALUES
                     (?)'''
        cur.execute(first, (0,))
        return_vals.append('0')
    else:
        return_vals.append(values[0])
    values=[]
    for row in cur.execute("SELECT dg2fuel FROM other_report"):
        values.append(row[0])
    if values==[]:
        first = '''INSERT INTO other_report (
                     dg2fuel
                       )
                    VALUES
                     (?)'''
        cur.execute(first, (0,))
        return_vals.append('0')
    else:
        return_vals.append(values[0])
    connect.commit()
    connect.close()
    return(return_vals)


def add_dg1rh(value):
    get_dg_values()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_dg1rh(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set dg1rh=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()

def add_dg2rh(value):
    get_dg_values()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_dg2rh(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set dg2rh=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()

def add_dg1fuel(value):
    get_dg_values()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_dg1fuel(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set dg1fuel=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()

def add_dg2fuel(value):
    get_dg_values()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_dg2fuel(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set dg2fuel=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()

def dg_rhs():
    points=get_dg_values()
    dg1rhs=int(points[0])*12
    dg2rhs=int(points[1])*12
    return(dh1rhs,dg2rhs)    
    
def add_pf_values(pf_value,pf_count):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_pf_values(pf_value,pf_count)
    cur = connect.cursor()
    existing=get_existing_PF_sum()
    pfsum=existing[0]
    pfcount=existing[1]
    new_pf=pf_value
    new_count=float(existing[1])+float(pf_count)
    first = '''UPDATE other_report set pf_sum=?,pf_count=?'''
    cur.execute(first, (new_pf,new_count,))
    connect.commit()
    connect.close()

def get_overall_pf():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_overall_pf()
    cur = connect.cursor()
    values=[]
    vall=get_existing_PF_sum()
    try:
        return(float(vall[0]))
        first = '''UPDATE other_report set pf_sum=?,pf_count=?'''
        cur.execute(first, ('0','0',))
        connect.commit()
        connect.close()
    except:
        return(0)

def get_cases():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_cases()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT cases FROM other_report"):
        values.append(str(row[0]))
    if values==[]:
        first = '''INSERT INTO other_report (
                     cases
                       )
                    VALUES
                     (?)'''
        cur.execute(first, (0,))
        connect.commit()
        connect.close()
        return('0')
    connect.close()
    return(values)

def add_cases(value):
    get_cases()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_cases(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set cases=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()

def get_chilled():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_chilled()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT chilled_blend FROM other_report"):
        values.append(str(row[0]))
    if values==[]:
        first = '''INSERT INTO other_report (
                     chilled_blend
                       )
                    VALUES
                     (?)'''
        cur.execute(first, ('0',))
        connect.commit()
        connect.close()
        return('0')
    connect.close()
    return(values)

def add_chilled(value):
    get_chilled()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_chilled(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set chilled_blend=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()

def get_unchilled():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_unchilled()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT unchilled_blend FROM other_report"):
        values.append(str(row[0]))
    if values==[]:
        first = '''INSERT INTO other_report (
                     unchilled_blend
                       )
                    VALUES
                     (?)'''
        cur.execute(first, ('0',))
        connect.commit()
        connect.close()
        return('0')
    connect.close()
    return(values)

def add_unchilled(value):
    get_unchilled()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_unchilled(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set unchilled_blend=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()

def get_manpower():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_manpower()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT manpower FROM other_report"):
        values.append(str(row[0]))
    if values==[]:
        first = '''INSERT INTO other_report (
                     manpower
                       )
                    VALUES
                     (?)'''
        cur.execute(first, ('0',))
        connect.commit()
        connect.close()
        return('0')
    connect.close()
    return(values)

def add_manpower(value):
    get_manpower()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_manpower(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set manpower=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()
    
def get_mdi():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_mdi()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT mdi FROM other_report"):
        values.append(str(row[0]))
    if values==[]:
        first = '''INSERT INTO other_report (
                     mdi
                       )
                    VALUES
                     (?)'''
        cur.execute(first, ('0',))
        connect.commit()
        connect.close()
        return('0')
    connect.close()
    return(values[0])

def add_mdi(value):
    get_mdi()
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_mdi(value)
    cur = connect.cursor()
    first = '''UPDATE other_report set mdi=?'''
    cur.execute(first, (value,))
    connect.commit()
    connect.close()
    
def unit_per_case():
    cases=get_cases()
    units=get_total_kwhr()[0]
    try:
        value=float(units)/float(cases[0])
    except:
        return('0')
    return(value)

def unit_per_chilled():
    chilled=get_chilled()
    units=get_total_kwhr()[0]
    try:
        value=float(units)/float(chilled[0])
    except:
        return('0')
    return(value)

def unit_per_unchilled():
    unchilled=get_unchilled()
    units=get_total_kwhr()[0]
    try:
        value=float(units)/float(unchilled[0])
    except:
        return('0')
    return(value)

def unit_per_manpower():
    manpower=get_manpower()
    units=get_total_kwhr()[0]
    try:
        value=float(manpower[0])/float(units)
    except:
        return('0')
    return(value)


#print(unit_per_case())
def add_to_kwh(data):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_to_kwh(data)
    cur = connect.cursor()
    date_now=datetime.datetime.now().date()
    data_new=[date_now]+data
    first = '''INSERT INTO consumed_kwh VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cur.execute(first, data_new)
    connect.commit()
    connect.close()


def add_to_kwh_daily(data):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_to_kwh_daily(data)
    cur = connect.cursor()
    date_now=datetime.datetime.now().date()
    data_new=[date_now]+data
    first = '''INSERT INTO daily_consumed_kwh VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cur.execute(first, data_new)
    connect.commit()
    connect.close()


def add_to_kvah(data):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_to_kvah(data)
    cur = connect.cursor()
    date_now=datetime.datetime.now().date()
    data_new=[date_now]+data
    first = '''INSERT INTO consumed_kvah VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cur.execute(first, data_new)
    connect.commit()
    connect.close()

    
def shift_to_main():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        shift_to_main()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT * FROM kwhreport"):
        values.append(row)
    print(values)
    kwhr=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]
    #kvahr=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]
    for i in values:
        print(i)
        index=(int(i[1])-1)
        kwhr[index]=float(i[4])
        #kvahr[index]=float(i[7])
    connect.close()
    add_to_kwh(kwhr)
    #add_to_kvah(kvahr)
    return(values)


def shift_to_main_daily():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        shift_to_main_daily()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT * FROM kwhreport_daily"):
        values.append(row)
    kwhr=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]
    #kvahr=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]
    for i in values:
        print(i)
        index=(int(i[1])-1)
        kwhr[index]=float(i[4])
        #kvahr[index]=float(i[7])
    connect.close()
    add_to_kwh_daily(kwhr)
    #add_to_kvah(kvahr)
    return(values)


def shift_to_main():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        shift_to_main()
    cur = connect.cursor()
    values=[]
    for row in cur.execute("SELECT * FROM kwhreport"):
        values.append(row)
    kwhr=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]
    #kvahr=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]
    for i in values:
        index=(int(i[1])-1)
        kwhr[index]=float(i[4])
        #kvahr[index]=float(i[7])
    connect.close()
    add_to_kwh(kwhr)
    #add_to_kvah(kvahr)
    return(values)


def add_to_overall():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_to_overall()
    cur = connect.cursor()
    date_now=datetime.datetime.now().date()
    total_units=get_total_kwhr()[0]
    pf=get_overall_pf()
    solar=get_total_kwhr()[1]
    cases=get_cases()[0]
    chilled=get_chilled()[0]
    unchilled=get_unchilled()[0]
    manpower=get_manpower()[0]
    upc=unit_per_case()
    upch=unit_per_chilled()
    upuch=unit_per_unchilled()
    upm=unit_per_manpower()
    mdi=get_mdi()
    dgs=get_dg_values()
    dg1units=get_total_kwhr()[3]
    dg2units=get_total_kwhr()[4]
    try:
        dg1eff=float(dg1units)/float(dgs[2])
    except:
        dg1eff='0'
    try:
        dg2eff=float(dg2units)/float(dgs[3])
    except:
        dg2eff='0'

    data_new=[date_now,total_units,pf,solar,cases,chilled,unchilled,manpower,upc,upch,upuch,upm,mdi,dgs[0],dgs[1],dgs[2],dgs[3],dg1eff,dg2eff,dg1units,dg2units]
    print(data_new)
    first = '''INSERT INTO overall VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cur.execute(first, data_new)
    connect.commit()
    connect.close()

   
def get_all_overall():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_all_overall()
    cur = connect.cursor()
    values=[]
    values1=[]
    for row in cur.execute("SELECT * FROM overall"):
        values.append(row)
    connect.close()
    return(values)


def get_all_from_daily():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_all_from_daily()
    cur = connect.cursor()
    values=[]
    values1=[]
    for row in cur.execute("SELECT * FROM consumed_kwh"):
        values.append(row)
    connect.close()
    return(values)


def get_all_from_daily_kwh():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_all_from_daily_kwh()
    cur = connect.cursor()
    values=[]
    values1=[]
    for row in cur.execute("SELECT * FROM daily_consumed_kwh"):
        values.append(row)
    connect.close()
    return(values)


def add_rated():
    all_kwh=get_all_from_daily()
    thr=get_all_thresholds()
    values=[]
    a=0
    for i in all_kwh:
        c=[]
        c.append(i[0])
        for j in range(0,len(i)-1):
            c.append(str(float(i[j+1])-float(thr[j])))
        add_all_rated(c)


def get_all_from_rated():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_all_from_rated()
    cur = connect.cursor()
    values=[]
    values1=[]
    for row in cur.execute("SELECT * FROM rated_against"):
        values.append(row)
    connect.close()
    return(values)
    
               
def insert_dept(data):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        insert_dept(data)
    cur = connect.cursor()
    first = '''INSERT INTO departments (
                     dept_name,
                     head_name,
                     meters,
                     heademail
                       )
                    VALUES
                     (?,?,?,?)'''
    cur.execute(first, (data[0],data[1],data[2],data[3],))
    connect.commit()
    connect.close()


def insert_meters(data):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        insert_meters(data)
    cur = connect.cursor()
    first = '''INSERT INTO meters (
                     meter_name,
                     dept_name,
                     meter_id
                       )
                    VALUES
                     (?,?,?)'''
    cur.execute(first, (data[0],data[1],data[2],))
    connect.commit()
    connect.close()


def add_new_local_meter(ids,*mapid):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_new_local_meter(ids,*mapid)
    cur = connect.cursor()
    first = '''INSERT INTO devices (
                     slave_id
                       )
                    VALUES
                     (?)'''
    cur.execute(first,(ids,))
    if not mapid:
        pass
    else:
        cur.execute("UPDATE meters SET slave_id=? WHERE meter_id=?", (ids, mapid[0],))
    connect.commit()
    connect.close()


def add_mapping(meter,slave):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        add_mapping(meter,slave)
    cur = connect.cursor()
    cur.execute("UPDATE meters SET slave_id=? WHERE meter_id=?", (slave, meter,))
    connect.commit()
    connect.close()

    
def fetch_all_deptnames():
## this function get the names of all the departments and fetches  
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        fetch_all_deptnames()
    cur = connect.cursor()
    names=[]
    for row in cur.execute("SELECT dept_name FROM departments"):
        names.append(str(row[0]))
    deptass={}
    for i in names:
        temp=[]
        for j in cur.execute("SELECT meter_name FROM meters WHERE dept_name=?",(i,)):
            temp.append(str(j[0]))
        deptass[str(i)]=temp
    connect.commit()
    connect.close()
    return(names,deptass)


def fetch_all_meternames():
## this function gets the names of all the meter names
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        fetch_all_meternames()
    cur = connect.cursor()
    names=[]
    for row in cur.execute("SELECT meter_name FROM meters"):
        names.append(str(row[0]))
    connect.commit()
    connect.close()
    return(names)


def create_meter_tables():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        create_meter_tables()
    cur = connect.cursor()
    for i in range(0,28):
        query= '''CREATE TABLE IF NOT EXISTS table'''+ str(i)+'''
         (
         Date    char        NOT NULL,
         Time    char        NOT NULL,
         V1    char        NOT NULL,
         V2    char        NOT NULL,
         V3    char        NOT NULL,
         Vavg    char        NOT NULL,
         V12    char        NOT NULL,
         V23    char        NOT NULL,
         V13    char        NOT NULL,
         Vpavg    char        NOT NULL,
         I1    char        NOT NULL,
         I2    char        NOT NULL,
         I3    char        NOT NULL,
         Iavg   char        NOT NULL,
         KW1    char        NOT NULL,
         KW2    char        NOT NULL,
         KW3    char        NOT NULL,
         KVA1    char        NOT NULL,
         KVA2    char        NOT NULL,
         KVA3    char        NOT NULL,
         KVAr1    char        NOT NULL,
         KVAr2    char        NOT NULL,
         KVAr3    char        NOT NULL,
         Total_KW    char        NOT NULL,
         Total_KVA    char        NOT NULL,
         Total_KVAr    char        NOT NULL,
         PF1   char        NOT NULL,
         PF2    char        NOT NULL,
         PF3    char        NOT NULL,
         PFavg   char        NOT NULL,
         Freq    char        NOT NULL,
         KWh    char        NOT NULL,
         MDI    char        NOT NULL
         )'''
        cur.execute(query)
    connect.close()
create_meter_tables()

def insert_values_meter(mid,value):
    new_value=[]
    #print("inserting values")
    #print(value)
    for i in range(len(value)):
        new_value.append(normalize(value[i]))
    connect = sqlite3.connect('data.db')
    cur = connect.cursor()
    date_now=str(datetime.datetime.now().date())
    time_now=str(datetime.datetime.now().time())
    values=[date_now,time_now]+new_value
    #print(len(values))
    try:
        first = '''INSERT INTO table'''+str(mid)+''' VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
        cur.execute(first, (values))
        connect.commit()
    except:
        time.sleep(5)
    connect.close()

def get_values_meter(mid):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_values_meter(mid)
    cur = connect.cursor()
    values=[]
    for i in cur.execute("SELECT * from table"+str(int(mid)-1)):
        values.append(i)        
    connect.close()
    return(values)

    
def get_all_slave_id():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_all_slave_id()
    cur = connect.cursor()
    ids=[]
    for row in cur.execute("SELECT slave_id FROM devices"):
        ids.append(int(str(row[0])))
    connect.commit()
    connect.close()
    return(ids)

def get_all_m_id():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_all_m_id()
    cur = connect.cursor()
    ids=[]
    for row in cur.execute("SELECT meter_id FROM meters"):
        ids.append(int(str(row[0])))
    connect.commit()
    connect.close()
    return(ids)

def slave_id_from_mid(m_id):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        slave_id_from_mid(m_id)
    cur = connect.cursor()
    names=[]
    for j in cur.execute("SELECT slave_id FROM meters WHERE meter_id=?",(m_id,)):
        names.append(j)
    connect.commit()
    connect.close()
    return(names[0][0])

def mid_from_slave(s_id):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        mid_from_slave(s_id)
    cur = connect.cursor()
    names=[]
    for j in cur.execute("SELECT meter_id FROM meters WHERE slave_id=?",(s_id,)):
        names.append(j)
    connect.commit()
    connect.close()
    return(names[0][0])

def mname_from_mid(m_id):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        mname_from_mid(m_id)
    cur = connect.cursor()
    names=[]
    for j in cur.execute("SELECT meter_name FROM meters WHERE meter_id=?",(m_id,)):
        names.append(j)
    connect.commit()
    connect.close()
    return(names[0][0])

def get_dept_from_mid(m_id):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        get_dept_from_mid(m_id)
    cur = connect.cursor()
    names=[]
    for j in cur.execute("SELECT dept_name FROM meters WHERE meter_id=?",(m_id,)):
        names.append(j)
    connect.commit()
    connect.close()
    return(str(names[0][0]))

def slave_id_from_mname(m_name):
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        slave_id_from_mname(m_name)
    cur = connect.cursor()
    names=[]
    for j in cur.execute("SELECT slave_id FROM meters WHERE meter_name=?",(m_name,)):
        names.append(j)
    connect.commit()
    connect.close()
    return(names[0][0])

def export_kwh_report():
    wb = xlsxwriter.Workbook('Reports/KPA/Daily KPA report'+str(datetime.datetime.now().date())+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+'.xlsx')
    ws1 = wb.add_worksheet("KWHr")
    ws2 = wb.add_worksheet("Rated against Load")
    ws3 = wb.add_worksheet("Ohers")
    ##export members list
    ws1.write(0, 0 ,'Departments')
    ws3.write(0, 0 ,'Consumed-Threshold')
    ws1.write(0, 1 ,'Date')
   ## ws2.write(column=1, row=1).value='Departments'
    ##ws2.write(column=1, row=2).value='Date'
    for i in range(1,29):
        name=mname_from_mid(i)
        dept=get_dept_from_mid(i)
        ws1.write(0 , i , str(dept))
        ws1.write(1 , i , str(name))
        ws2.write(0 , i , str(dept))
        ws2.write(1 , i , str(name))
    values=get_all_from_daily()
    rown=2
    for j in values:
        for i in range(len(j)):
            ws1.write(rown, i, str(j[i]))
        rown+=1
    values1=get_all_from_rated()
    rown=2
    for j in values1:
        for i in range(len(j)):
            ws2.write(rown , i , str(j[i]))
        rown+=1
    rown=1
    ws3.write(rown,0 , 'Date')
    ws3.write(rown,1 , 'Total Energy')
    ws3.write(rown,2 , 'Overall PF')
    ws3.write(rown,3 , 'Solar (Energy)')
    ws3.write(rown,4 , 'Cases Produced')
    ws3.write(rown,5 , 'Chilled Blend Produced(Kl)')
    ws3.write(rown,6 , 'Unchilled Blend Produced(Kl)')
    ws3.write(rown,7 , 'Manpower')
    ws3.write(rown,8 , 'Units per Case')
    ws3.write(rown,9 , 'Units per Kl of Chilled Blend')
    ws3.write(rown,10 , 'Units per Kl of Unchilled Blend')
    ws3.write(rown,11 , 'Manpower Per Unit')
    ws3.write(rown,12 , 'MDI')
    ws3.write(rown,13 , 'DG1 Run Hours')
    ws3.write(rown,14 , 'DG2 Run Hours')
    ws3.write(rown,15 , 'DG1 Fuel')
    ws3.write(rown,16 , 'DG2 Fuel')
    ws3.write(rown,17 , 'DG1 Efficiency')
    ws3.write(rown,18 , 'DG2 Efficiency')
    ws3.write(rown,19 , 'DG1 Units')
    ws3.write(rown,20 , 'DG2 Units')
    values=get_all_overall()
    rown=2
    for i in values:
        for j in range(len(i)):
            ws3.write(rown,j,i[j])
        rown+=1
    #wb.save('Reports/KPA/Daily KPA report'+str(datetime.datetime.now().date())+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+'.xlsx')
    wb.close()

def export_kwh_report_monthly():
    wb = xlsxwriter.Workbook('Reports/KPA/Monthly KPA report'+str(datetime.datetime.now().date())+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+'.xlsx')
    ws1 = wb.add_worksheet("KWHr")
    ws2 = wb.add_worksheet("Rated against Load")
    ws3 = wb.add_worksheet("Ohers")
    ##export members list
    ws1.write(0, 0 ,'Departments')
    ws3.write(0, 0 ,'Consumed-Threshold')
    ws1.write(0, 1 ,'Date')
   ## ws2.write(column=1, row=1).value='Departments'
    ##ws2.write(column=1, row=2).value='Date'
    for i in range(1,29):
        name=mname_from_mid(i)
        dept=get_dept_from_mid(i)
        ws1.write(0 , i , str(dept))
        ws1.write(1 , i , str(name))
        ws2.write(0 , i , str(dept))
        ws2.write(1 , i , str(name))
    values=get_all_from_daily()
    rown=2
    for j in values:
        for i in range(len(j)):
            ws1.write(rown, i, str(j[i]))
        rown+=1
    values1=get_all_from_rated()
    rown=2
    for j in values1:
        for i in range(len(j)):
            ws2.write(rown , i , str(j[i]))
        rown+=1
    rown=1
    ws3.write(rown,0 , 'Date')
    ws3.write(rown,1 , 'Total Energy')
    ws3.write(rown,2 , 'Overall PF')
    ws3.write(rown,3 , 'Solar (Energy)')
    ws3.write(rown,4 , 'Cases Produced')
    ws3.write(rown,5 , 'Chilled Blend Produced(Kl)')
    ws3.write(rown,6 , 'Unchilled Blend Produced(Kl)')
    ws3.write(rown,7 , 'Manpower')
    ws3.write(rown,8 , 'Units per Case')
    ws3.write(rown,9 , 'Units per Kl of Chilled Blend')
    ws3.write(rown,10 , 'Units per Kl of Unchilled Blend')
    ws3.write(rown,11 , 'Manpower Per Unit')
    ws3.write(rown,12 , 'MDI')
    ws3.write(rown,13 , 'DG1 Run Hours')
    ws3.write(rown,14 , 'DG2 Run Hours')
    ws3.write(rown,15 , 'DG1 Fuel')
    ws3.write(rown,16 , 'DG2 Fuel')
    ws3.write(rown,17 , 'DG1 Efficiency')
    ws3.write(rown,18 , 'DG2 Efficiency')
    ws3.write(rown,19 , 'DG1 Units')
    ws3.write(rown,20 , 'DG2 Units')
    values=get_all_overall()
    rown=2
    for i in values:
        for j in range(len(i)):
            ws3.write(rown,j,i[j])
        rown+=1
    #wb.save('Reports/KPA/Daily KPA report'+str(datetime.datetime.now().date())+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+'.xlsx')
    wb.close()



def export_kwh_report_daily():
    wb = xlsxwriter.Workbook('Reports/KWH/Daily KWH report'+str(datetime.datetime.now().date())+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+'.xlsx')
    ws1 = wb.add_worksheet("KWHr")

    ##export members list
    ws1.write(0 , 0 , 'Departments')
    ws1.write(1 , 1 , 'Date')
   ## ws2.write(column=1, row=1).value='Departments'
    ##ws2.write(column=1, row=2).value='Date'
    for i in range(1,29):
        name=mname_from_mid(i)
        dept=get_dept_from_mid(i)
        ws1.write(0 , i , str(dept))
        ws1.write(1 , i , str(name))
    values=get_all_from_daily_kwh()
    rown=2
    for j in values:
        for i in range(len(j)):
            ws1.write(rown , i , str(j[i]))
        rown+=1
    wb.close()
    #wb.save('Reports/KWH/Daily KWH report'+str(datetime.datetime.now().date())+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+'.xlsx')




def export_manual_report():
    wb = xlsxwriter.Workbook('Reports/Manual/Report'+str(datetime.datetime.now().date())+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+'.xlsx')
    wsa=[]
    mids=get_all_m_id()
    for i in mids:
        name=mname_from_mid(i)
        try:
            wsa.append(wb.add_worksheet(str(name)))
        except Exception as e:
            wsa.append(wb.add_worksheet(str(name)+'1'))
            
        wsa[int(i)-1].write(0,0,'Date')
        wsa[int(i)-1].write(0,1,'Time')
        wsa[int(i)-1].write(0,2,'V1')
        wsa[int(i)-1].write(0,3,'V2')
        wsa[int(i)-1].write(0,4,'V3')
        wsa[int(i)-1].write(0,5,'Vavg')
        wsa[int(i)-1].write(0,6,'V12')
        wsa[int(i)-1].write(0,7,'V23')
        wsa[int(i)-1].write(0,8,'V13')
        wsa[int(i)-1].write(0,9,'Vpavg')
        wsa[int(i)-1].write(0,10,'I1')
        wsa[int(i)-1].write(0,11,'I2 ')
        wsa[int(i)-1].write(0,12,'I3')
        wsa[int(i)-1].write(0,13,'Iavg')
        wsa[int(i)-1].write(0,14,'KW1')
        wsa[int(i)-1].write(0,15,'KW2')
        wsa[int(i)-1].write(0,16,'KW3')
        wsa[int(i)-1].write(0,17,'KVA1')
        wsa[int(i)-1].write(0,18,'KVA2')
        wsa[int(i)-1].write(0,19,'KVA3')
        wsa[int(i)-1].write(0,20,'KVAr1')
        wsa[int(i)-1].write(0,21,'KVAr2')
        wsa[int(i)-1].write(0,22,'KVAr3')
        wsa[int(i)-1].write(0,23,'Total_KW')
        wsa[int(i)-1].write(0,24,'Total_KVA')
        wsa[int(i)-1].write(0,25,'Total_KVAr')
        wsa[int(i)-1].write(0,26,'PF1')
        wsa[int(i)-1].write(0,27,'PF2')
        wsa[int(i)-1].write(0,28,'PF3')
        wsa[int(i)-1].write(0,29,'PFavg')
        wsa[int(i)-1].write(0,30,'Freq')
        wsa[int(i)-1].write(0,31,'KWh')
        all_data=get_values_meter(i)
        print(all_data)
        r=1
        for j in all_data:
            for k in range(0,32):
                wsa[int(i)-1].write(r , k , j[k])
            r+=1
    wb.close()
    #wb.save('Reports/Manual/Report'+str(datetime.datetime.now().date())+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+'.xlsx')


def drop_tables():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        drop_tables()
    cur = connect.cursor()
    cur.execute('''DROP TABLE IF EXISTS  kwhreport''')        
    connect.commit()
    connect.close()

def drop_tables_daily_kwh():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        drop_tables_daily()
    cur = connect.cursor()
    cur.execute('''DROP TABLE IF EXISTS  kwhreport_daily''')        
    connect.commit()
    connect.close()

def drop_tables_daily():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        drop_tables_daily()
    cur = connect.cursor()
    mids=get_all_m_id()
    for i in mids:
        cur.execute('''DROP TABLE IF EXISTS  table'''+str(int(i)-1))
    connect.commit()
    connect.close()
        
def send_mail():
    pass
def cleardgrh():
    add_dg1rh('0')
    add_dg2rh('0')
    
def generate_report_daily():
    shift_to_main()
    add_to_overall()
    add_rated()
    export_kwh_report()
    drop_tables()
    create()
    create_meter_tables()
    cleardgrh()

def generate_manual_report():
    export_manual_report()


def generate_daily_kwh_rpt():
    shift_to_main_daily()
    export_kwh_report_daily()
    drop_tables_daily_kwh()
    create()

def drop_monthly_tables():
    try:
        connect = sqlite3.connect('data.db')
    except:
        time.sleep(20)
        drop_monthly_tables()
    cur = connect.cursor()
    cur.execute('''DROP TABLE IF EXISTS  consumed_kwh''')
    cur.execute('''DROP TABLE IF EXISTS  daily_consumed_kwh''')
    cur.execute('''DROP TABLE IF EXISTS  overall''')
    cur.execute('''DROP TABLE IF EXISTS  rated_against''')
    connect.commit()
    connect.close()
    
    
def generate_report_monthly():   
    ##shift_to_main()
    ##add_to_overall()
    ##add_rated()
    export_kwh_report_monthly()
    drop_tables()
    drop_monthly_tables()
    create()
    #drop_tables_meters()
    create_meter_tables()
    cleardgrh()

#generate_manual_report()         
    

