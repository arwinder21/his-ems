from tkinter import *
class multi_frame():
    def __init__(self,root,photo):
        self._root=root
        self._photo=photo
    def create(self,num_frames):
        self._frames=[]
        self._nums=num_frames
        for i in range(num_frames):
            self._frames.append(Frame(self._root,background='springgreen1',width=600,height=300))
            lbl1=Label(self._frames[i],image=self._photo)
            lbl1.place(x=0, y=0)
        return(self._frames)
    def pack_one(self,num,coords):
        self._curr_pack=num
        for i in range(len(self._frames)):
            if i==self._curr_pack:
                self._frames[self._curr_pack].configure(width=600,height=300)
                self._frames[self._curr_pack].grid(row=coords[0],column=coords[1],padx=220,pady=100)
                self._frames[self._curr_pack].grid_propagate(False)
            else:
                self._frames[i].grid_forget()


