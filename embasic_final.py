from tkinter import *
import serial
import serial.tools.list_ports as list_ports
import minimalmodbus
import json
from time import sleep
import urllib2
import os.path,winshell
from win32com.client import Dispatch
import requests
import socket
import datetime

import ctypes, sys
run_flag=0
def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False
def main_check():
    global run_flag
    if is_admin():
        #print("inside is admin")
        # Code of your program here
        from win32com.shell import shell, shellcon
        import subprocess
        import os

        import win32com.shell.shell as shell
        import winshell, admin


        def startupdirectory():

            path = shell.SHGetFolderPath(
                0,
                shellcon.CSIDL_COMMON_STARTUP,
                0,  # null access token (no impersonation)
                0  # want current value, shellcon.SHGFP_TYPE_CURRENT isn't available, this seems to work
            )
            print(path)
            shortcut = winshell.shortcut(os.path.realpath('__file__'))
            shortcut.working_directory = os.path.realpath('__file__')
            shortcut.write(os.path.join(path, "embasic.lnk"))
            shortcut.dump()

        run_flag=1
        print(run_flag)
        startupdirectory()
    else:
        # Re-run the program with admin rights
        ctypes.windll.shell32.ShellExecuteW(None, u"runas", unicode(sys.executable), unicode('__file__'), None, 1)
        run_flag=1
       
main_check()   
print(run_flag)
#file_path="C:\Users\Arwinder\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\Dropbox"
#print(os.path.islink(file_path))
#print(os.stat(file_path))
#print(connected)
#print(os.path.realpath(__file__))
if run_flag==1:
    def is_connected():
        try:
            # connect to the host -- tells us if the host is actually
            # reachable
            socket.create_connection(("www.google.com", 80))
            return True
        except OSError:
            pass
        return False
    print(is_connected())
    #comport=raw_input("Enter the comport")
    slaveid=1
    def initiate(slaveid):
            comlist = serial.tools.list_ports.comports()
            connected = []
            for element in comlist:
                connected.append(element.device)
            print(connected[0])
            try:
                instrument=minimalmodbus.Instrument(connected[0], slaveid)

                instrument.serial.baudrate = 9600
                instrument.serial.bytesize = 8
                instrument.serial.parity = serial.PARITY_NONE
                instrument.serial.stopbits = 1
                instrument.serial.timeout = 200
                instrument.serial.close_after_each_call = TRUE
                print('Initialization sucessful')
                return(instrument)
            except:
                
                print('Initialization Failed,Trying again,will retry after 10 sec')
                sleep(10)
               ## try:
                ##instrument.serial.close()
    ##            except:
    ##                pass
                initiate(slaveid)
    ins=initiate(slaveid)
    param_ids=["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27"]
    param_names=["Temprature1","Temprature2","Flow","KW(H)","KWHr(H)","KW(C)","KWHr(C)"]

    while(1):
        data_temp=[]
        print("inside while loop")
        data_temp.append(ins.read_float(1,4,2))
        data_temp.append(ins.read_float(3,4,2))
        data_temp.append(ins.read_float(5,4,2))
        data_temp.append(ins.read_float(7,4,2))
        data_temp.append(ins.read_float(11,4,2))
        data_temp.append(ins.read_float(13,4,2))
        data_temp.append(ins.read_float(15,4,2))
        data_temp.append(ins.read_float(17,4,2))
        data_temp.append(ins.read_float(19,4,2))
        data_temp.append(ins.read_float(21,4,2))
        data_temp.append(ins.read_float(23,4,2))
        data_temp.append(ins.read_float(25,4,2))
        data_temp.append(ins.read_float(27,3,2))
        data_temp.append(ins.read_float(29,3,2))
        data_temp.append(ins.read_float(31,3,2))
        data_temp.append(ins.read_float(33,3,2))
        data_temp.append(ins.read_float(35,3,2))
        data_temp.append(ins.read_float(37,3,2))
        data_temp.append(ins.read_float(39,3,2))
        data_temp.append(ins.read_float(41,3,2))
        data_temp.append(ins.read_float(43,3,2))
        data_temp.append(ins.read_float(45,3,2))
        data_temp.append(ins.read_float(47,3,2))
        data_temp.append(ins.read_float(49,3,2))
        data_temp.append(ins.read_float(51,3,2))
        data_temp.append(ins.read_float(53,3,2))
        data_temp.append(ins.read_float(55,3,2))
        data_temp.append(ins.read_float(57,3,2))
        data_temp.append(ins.read_float(59,3,2))
        data_temp.append(ins.read_float(61,3,2))
        data_temp.append(ins.read_float(63,3,2))
        print(data_temp)
        for i in range(0,len(data_temp)):
            data=data_temp[i]*1000
            data=int(data)
            data=float(data)
            data_temp[i]=data/1000
        timestamp=str(datetime.datetime.now())
        #print(data_temp)

        url = 'http://industrylog.org/emsapi/REST/loggings/'
        payload = [{"param_id": param_ids[0], "param_value": str(data_temp[0]), "timestamp1": timestamp},\
                   {"param_id": param_ids[1], "param_value": str(data_temp[1]),"timestamp1": timestamp},\
                   {"param_id": param_ids[2], "param_value": str(data_temp[2]),"timestamp1": timestamp},\
                   {"param_id": param_ids[3], "param_value": str(data_temp[3]),"timestamp1": timestamp},\
                   {"param_id": param_ids[4], "param_value": str(data_temp[4]),"timestamp1": timestamp},\
                   {"param_id": param_ids[5], "param_value": str(data_temp[5]),"timestamp1": timestamp},\
                   {"param_id": param_ids[6], "param_value": str(data_temp[6]),"timestamp1": timestamp},\
                   {"param_id": param_ids[7], "param_value": str(data_temp[7]),"timestamp1": timestamp},\
                   {"param_id": param_ids[8], "param_value": str(data_temp[8]),"timestamp1": timestamp},\
                   {"param_id": param_ids[9], "param_value": str(data_temp[9]),"timestamp1": timestamp},\
                   {"param_id": param_ids[10], "param_value": str(data_temp[10]),"timestamp1": timestamp},\
                   {"param_id": param_ids[11], "param_value": str(data_temp[11]),"timestamp1": timestamp},\
                   {"param_id": param_ids[12], "param_value": str(data_temp[12]),"timestamp1": timestamp},\
                   {"param_id": param_ids[13], "param_value": str(data_temp[13]),"timestamp1": timestamp},\
                   {"param_id": param_ids[14], "param_value": str(data_temp[14]),"timestamp1": timestamp},\
                   {"param_id": param_ids[15], "param_value": str(data_temp[15]),"timestamp1": timestamp},\
                   {"param_id": param_ids[16], "param_value": str(data_temp[16]),"timestamp1": timestamp},\
                   {"param_id": param_ids[17], "param_value": str(data_temp[17]),"timestamp1": timestamp},\
                   {"param_id": param_ids[18], "param_value": str(data_temp[18]),"timestamp1": timestamp},\
                   {"param_id": param_ids[19], "param_value": str(data_temp[19]),"timestamp1": timestamp},\
                   {"param_id": param_ids[20], "param_value": str(data_temp[20]),"timestamp1": timestamp},\
                   {"param_id": param_ids[21], "param_value": str(data_temp[21]),"timestamp1": timestamp},\
                   {"param_id": param_ids[22], "param_value": str(data_temp[22]),"timestamp1": timestamp},\
                   {"param_id": param_ids[23], "param_value": str(data_temp[23]),"timestamp1": timestamp},\
                   {"param_id": param_ids[24], "param_value": str(data_temp[24]),"timestamp1": timestamp},\
                   {"param_id": param_ids[25], "param_value": str(data_temp[25]),"timestamp1": timestamp},\
                   {"param_id": param_ids[26], "param_value": str(data_temp[26]),"timestamp1": timestamp},\
                   {"param_id": param_ids[27], "param_value": str(data_temp[27]),"timestamp1": timestamp},\
                   {"param_id": param_ids[28], "param_value": str(data_temp[4]),"timestamp1": timestamp},\
                   {"param_id": param_ids[29], "param_value": str(data_temp[4]),"timestamp1": timestamp},\
                   {"param_id": param_ids[30], "param_value": str(data_temp[4]),"timestamp1": timestamp},\
                   {"param_id": param_ids[31], "param_value": str(data_temp[4]),"timestamp1": timestamp}\
                   ]
        print(payload[0])
        for i in range(0,len(payload)):
            r = requests.post(url, json=(payload[i]))
            
        print(data_temp)
        sleep(20)
else:
    print("Please rerun as admin")
    raw_input("Press any key to exit")

