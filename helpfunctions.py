import db
import classes
def init_db(comport):
    db.create_all()
    block_data=['Block_name',0,comport]
    db.add_block(block_data)
init_db('COM7')

def edit_parameter(lbls,i):
    edit_class=classes.edit_parameter(lbls,i)
    edit_class.draw_edit_box()

