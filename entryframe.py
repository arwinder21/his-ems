from tkinter import *
class entry_table():
    def __init__(self,master,rows,columns):
        self._master=master
        self._rows=rows
        self._columns=columns
        self._entry=[]
        self._count=0
    def draw_table(self):
        self._toplvl=Toplevel(self._master)
        for i in range(0,self._rows):
            for j in range(0,self._columns):
                self._entry.append(Entry(self._toplvl))
                self._entry[self._count].grid(row=i,column=j)
                self._count=self._count+1
    def draw_table_main(self):
        for i in range(0,self._rows):
            for j in range(0,self._columns):
                self._entry.append(Entry(self._master))
                self._entry[self._count].grid(row=i,column=j)
                self._count=self._count+1
        
    def add_data(self,row,column,data):
        coordinate=(self._columns*row)+column
        self._entry[coordinate].insert(0,data)

    def make_ro_all(self):
        count=0
        for i in range(0,self._rows):
            for j in range(0,self._columns):
                self._entry[count].config(state='readonly')
                count=count+1
    def make_ro(self,row,column):
        coor=coordinate=(self._columns*row)+column
        self._entry[coor].config(state='readonly')
        
    def add_row(self,row,data):
        count_basic=self._columns*row
        #print(len(self._entry))
        for i in range(0,self._columns):
            self._entry[count_basic+i].insert(0,data[i])

