import dummymodbus as minimalmodbus
import serial
import serial.tools.list_ports as list_ports
import time
import threading
import apis
import time
comlist = serial.tools.list_ports.comports()
connected = []
for element in comlist:
    connected.append(element.device)
print(connected)
def initiate(connected,slaveid):
    inst=minimalmodbus.Instrument(connected, slaveid)
##    inst.serial.baudrate = 9600
##    inst.serial.bytesize = 8
##    inst.serial.parity=serial.PARITY_NONE
##    inst.serial.stopbits = 1
##    inst.serial.timeout = 0.1
##    inst.serial.close_after_each_call = True
    inst.baudrate = 9600
    inst.bytesize = 8
    inst.parity=serial.PARITY_NONE
    inst.stopbits = 1
    inst.timeout = 1
    inst.close_after_each_call = True
    return(inst)
meters=[]
def initiate_all(num,slave_id,connected):
    global meters
    for i in range(0,num):
        meters.append(initiate(connected,slave_id[i]))
    return(meters)
def read_float_all(meter):
    values=[]
    for i in range(1,33):
        time.sleep(0.1)
        try:
            values.append(meter.read_float((2*i-1),4,2))
        except:
            values.append('error')
            #print('error')
        
    return(values)

slave_id=apis.get_all_slave_id()
#c=initiate_all(26,slave_id,connected[0])
c=initiate_all(26,slave_id,'connected')
all_values={}
all_threads=[]
value_present=False

for i in slave_id:
    all_values[str(i)]='NULL'
for i in slave_id:
    
def get_values():
    global c,all_values,slave_id,value_present
    while(1):
        for i in range(len(slave_id)):
            #print(all_values)
            try:
                all_values[str(slave_id[i])]=read_float_all(c[slave_id[i]-1])
            except:
                print('exception in getting values')
        print(all_values)
        
##def get_values_dummy():
##    all_values['1']=['V1','V2','V3','Vavg','V12','V13','V23','Vnavg','I1','I2','I3','Iavg','KW1','KW2','KW3','KVA1','KVA2','KVA3','KVAr1','KVAr2','KVAr3','TKW','TKVA','TKVAr','PF1','PF2','PF3','APF','FREQ','KWh','KVAh','KVArh']
##    
def fetch_values():
    global all_values
    
    return(all_values)
            #print(all_values)
mdi=0
def mdi_calc():
    global all_values,mdi
    while (1):
        total_sum=0
        count=0
        m_name='Mains'
        slave=apis.slave_id_from_mname(m_name)
        start_time=time.time()
        while(1):
            try:
                now_time=time.time()
                elapsed=int(now_time)-int(start_time)
                if elapsed<300:
                    kw=all_values[str(slave)][21]
                    total_sum+=float(kw)
                    count+=1
                else:
                    mdi_temp=total_sum/count
                    if mdi_temp>mdi:
                        mdi=mdi_temp
                    else:
                        pass
                    #print(mdi)
                    break
            except:
                #print('exception',all_values[str(slave)])
                continue
                    
                    
def get_mdi():
    global mdi
    return(mdi)

    
##all_threads=(threading.Thread(target=get_values))
##all_threads.start()
##mdi_threads=(threading.Thread(target=mdi_calc))
##mdi_threads.start()
##    

