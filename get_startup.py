from win32com.shell import shell, shellcon
import subprocess
import os

import win32com.shell.shell as shell
import winshell,admin


def startupdirectory():

    path =shell.SHGetFolderPath(
        0,
        shellcon.CSIDL_COMMON_STARTUP,
        0,  # null access token (no impersonation)
        0  # want current value, shellcon.SHGFP_TYPE_CURRENT isn't available, this seems to work
    )
    shortcut = winshell.shortcut(os.path.realpath(__file__))
    shortcut.working_directory = os.path.realpath(__file__)
    shortcut.write(os.path.join(path, "get_startup.lnk"))
    shortcut.dump()

if __name__ == '__main__':
    print startupdirectory()