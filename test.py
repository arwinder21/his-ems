import minimalmodbus
import serial
import serial.tools.list_ports as list_ports
import time
import threading
import apis
import time
comlist = serial.tools.list_ports.comports()
connected = []
for element in comlist:
    connected.append(element.device)
print(connected)
def initiate(connected,slaveid):
    inst=minimalmodbus.Instrument(connected, slaveid)
    inst.serial.baudrate = 9600
    inst.serial.bytesize = 8
    inst.serial.parity=serial.PARITY_NONE
    inst.serial.stopbits = 1
    inst.serial.timeout = 0.1
    inst.serial.close_after_each_call = True
    return(inst)

inst=initiate(connected[0],22)
i=0
while(i<20):
    print(inst.read_float(59,4,2))
    time.sleep(1)
    print(inst.read_float(61,4,2))
    time.sleep(1)
    i+=1
