import ctypes, sys

def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False

if is_admin():
    # Code of your program here
    from win32com.shell import shell, shellcon
    import subprocess
    import os

    import win32com.shell.shell as shell
    import winshell, admin


    def startupdirectory():

        path = shell.SHGetFolderPath(
            0,
            shellcon.CSIDL_COMMON_STARTUP,
            0,  # null access token (no impersonation)
            0  # want current value, shellcon.SHGFP_TYPE_CURRENT isn't available, this seems to work
        )
        shortcut = winshell.shortcut(os.path.realpath(__file__))
        shortcut.working_directory = os.path.realpath(__file__)
        shortcut.write(os.path.join(path, "get_startup.lnk"))
        shortcut.dump()


    startupdirectory()
else:
    # Re-run the program with admin rights
    ctypes.windll.shell32.ShellExecuteW(None, u"runas", unicode(sys.executable), unicode(__file__), None, 1)