from tkinter import *
class edit_parameter():
    def __init__(self,lbls,i):
        self._lbls=lbls
        self._i=i
    def dummy(self):
        print(self)
        print(self._lbls)
    def edit_lbl(self,name):
        extracted_data=[]
        for i in range(0,len(name)):
            extracted_data.append(name[i].get())
        self._lbls[self._i].config(text=extracted_data[0])
        print(extracted_data)

    def draw_edit_box(self):
        top_one=Toplevel()
        top_one.geometry('300x300')
        name=Label(top_one,text='Parameter Name')
        reg_type=Label(top_one,text='Register Type')
        reg_add=Label(top_one,text='Register Address')
        data_type=Label(top_one,text='Data Type')
        par_unit=Label(top_one,text='Parameter unit')
        log_rate=Label(top_one,text='Log Rate')
        log_unit=Label(top_one,text='Time Unit')
        canc_but=Button(top_one,text='Cancel',command=top_one.destroy)
        name_ent=Entry(top_one)
        reg_type_net=Entry(top_one)
        reg_add_ent=Entry(top_one)
        log_rate_ent=Entry(top_one)
        log_unit_ent=Entry(top_one)
        data_type_ent=Entry(top_one)
        par_unit_ent=Entry(top_one)
        all_data=[name_ent,reg_type_net,reg_add_ent,data_type_ent,par_unit_ent,log_rate_ent,log_unit_ent]
        ok_but = Button(top_one, text='Ok', command=lambda:self.edit_lbl(all_data))
        name.grid(row=0,column=0)
        name_ent.grid(row=0,column=1)
        reg_type.grid(row=1,column=0)
        reg_type_net.grid(row=1,column=1)
        reg_add.grid(row=2,column=0)
        reg_add_ent.grid(row=2,column=1)
        data_type.grid(row=3,column=0)
        data_type_ent.grid(row=3,column=1)
        par_unit.grid(row=4,column=0)
        par_unit_ent.grid(row=4,column=1)
        log_rate.grid(row=5,column=0)
        log_rate_ent.grid(row=5,column=1)
        log_unit.grid(row=6,column=0)
        log_unit_ent.grid(row=6,column=1)
        ok_but.grid(row=7,column=0)
        canc_but.grid(row=7,column=1)
        top_one.mainloop()
class edit_meter():
    def __init__(self):
        self._connect=['Even','Odd','None']
        self._baud_rate=[1200,2400,4800,9600,19200,38400,57600,115200]
    def draw_edit_box(self):
        top_one=Toplevel()
        com_port=Label(top_one,text='COM Port')
        slave_id=Label(top_one,text='Salve Id')
        baud_rate=Label(top_one,text='Baud Rate')
        parity=Label(top_one,text='Parity')
        stop_bit=Label(top_one,text='Stop Bit')
        time_out=Label(top_one,text='Time Out')
        com_port_combo=ttk.Combobox(top_one,values=self._connect)
        slave_id_ent=Entry(top_one)
        baud_rate_combo=ttk.Combobox(top_one,values=self._baud_rate)
        parity_combo=ttk.Combobox(top_one,values=self._connect)
        stop_bit_ent=Entry(top_one)
        time_out_ent=Entry(top_one)
        com_port.grid(row=0,column=0)
        com_port_combo.grid(row=0,column=1)
        slave_id.grid(row=1,column=0)
        slave_id_ent.grid(row=1,column=1)
        baud_rate.grid(row=2,column=0)
        baud_rate_combo.grid(row=2,column=1)
        parity.grid(row=3,column=0)
        parity_combo.grid(row=3,column=1)
        stop_bit.grid(row=4,column=0)
        stop_bit_ent.grid(row=4,column=1)
        time_out.grid(row=5,column=0)
        time_out_ent.grid(row=5,column=1)
        top_one.mainloop()

