import sqlite3
import time, datetime

def create_basic_tables():
    connect = sqlite3.connect('data.db')
    cur = connect.cursor()
    first = '''CREATE TABLE IF NOT EXISTS  Logging_pending
         (
         timestamp       char      NOT NULL,
         param_name      char      NOT NULL,
         param_value     char     NOT NULL,
         param_id        integer        NOT NULL

         )'''
    cur.execute(first)
    first = '''CREATE TABLE IF NOT EXISTS  Logging_done
         (
         timestamp       char      NOT NULL,
         param_name      char      NOT NULL,
         param_value     char     NOT NULL,
         param_id        integer        NOT NULL

         )'''
    cur.execute(first)

def add_logging_basic(data):
    connect = sqlite3.connect('data.db')
    cur = connect.cursor()
    timestamp=str(datetime.datetime.now())
    first = '''INSERT INTO Logging_pending (
                     timestamp,
                     param_name,
                     param_value,
                     param_id
                       )
                    VALUES
                     (?,?,?,?)'''
    cur.execute(first, (timestamp,data[0], data[1], data[2],))
    connect.commit()
    connect.close()
def fetch_clear():
    connect=sqlite3.connect('data.db')
    cur = connect.cursor()
    rowid=[]
    data=[]
    for row in cur.execute('SELECT * from Logging_pending'):
        data.append(row)

    for row in cur.execute('SELECT rowid from Logging_pending'):
        rowid.append(row)
    print(rowid)
    for i in range(0,len(rowid)):
        cur.execute('DELETE  FROM Logging_pending WHERE rowid=? ',rowid[i])
    connect.commit()
    return(data)

def send_logging_basic(data):
    pass
# create all new tables
def create_all():
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    first = '''CREATE TABLE IF NOT EXISTS  Blocks
            (
            block_id       integer      NOT NULL   PRIMARY KEY,
            block_name       char      NOT NULL,
            customers       integer      NOT NULL,
            comport      char         NOT NULL
            )'''
    cur.execute(first)

    first = '''CREATE TABLE IF NOT EXISTS  Customers
         (
         customer_name    char      NOT NULL   ,
         customer_id      integer      NOT NULL PRIMARY KEY,
         block_id       integer      NOT NULL,
         GST_Number     char     NOT NULL,
         address         char        NOT NULL,
         ph_no        char    NOT NULL,
         email_id    char    NOT NULL,
         meters       integer       NOT NULL,
                     FOREIGN KEY (block_id) REFERENCES Blocks(block_id)
            
         )'''
    cur.execute(first)
    first = '''CREATE TABLE IF NOT EXISTS  Meters
         (
         meter_id       integer      NOT NULL   PRIMARY KEY,
         meter_name       char      NOT NULL,
         slave_id       integer      NOT NULL,
         com_port       char        Not Null,
         baud_rate      integer     NOT NULL,
         parity         char        NOT NULL,
         stop_bit        integer    NOT NULL,
         time_out         integer    NOT NULL,
         customer_id        integer     NOT NULL,
         parameters      integer     NOT NULL,
                   FOREIGN KEY (customer_id) REFERENCES Customers(customer_id)
            
         )'''
    cur.execute(first)

    first = '''CREATE TABLE IF NOT EXISTS  Parameters
         (
         param_id       integer      NOT NULL   PRIMARY KEY,
         param_name       char      NOT NULL,
         param_type       char      NOT NULL,
         param_address      integer     NOT NULL,
         log_rate        integer    NOT NULL,
         log_unit         char    NOT NULL,
         meter_id      integer     NOT NULL,
                   FOREIGN KEY (meter_id) REFERENCES Meters(meter_id)
            
         )'''
    cur.execute(first)

    first = '''CREATE TABLE IF NOT EXISTS  Logging
         (
         timestamp       char      NOT NULL,
         param_name      char      NOT NULL,
         param_value     char     NOT NULL,
         param_id        integer        NOT NULL,
                  FOREIGN KEY (param_id) REFERENCES Parameters(param_id)
            
         )'''
    cur.execute(first)

    connect.close()



##block data part
##contains functions about the block editing

def check_block_duplicity(name):
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    check = False
    for row in cur.execute("SELECT block_name FROM Blocks"):
        if row[0] == name:
            # print('name already exists')
            check = True
            break
        else:
            print('Block already exists')
            continue
    return (check)


# data=['block_name',0,'comport']
def add_block(data):
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    value = check_block_duplicity(data[0])

    # print(value)
    if value == False:
        first = '''INSERT INTO Blocks (
                 block_name,
                 customers,
                 comport
                   )
                VALUES
                 (?,?,?)'''
        cur.execute(first, (data[0], data[1], data[2],))
        connect.commit()
    connect.close()


def get_all_block_names():
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    names = []
    for row in cur.execute("SELECT block_name FROM Blocks"):
        names.append(row)
    connect.close()
    return (names)


def get_block_id(name):
    #print(name)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    block_id = 'Null'
    for row in cur.execute("SELECT block_id FROM Blocks WHERE block_name=?", (name,)):
        # print(row)
        block_id = row
    connect.close()
    return (block_id)


def get_customers_block(block_id):
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    customers = 'Null'
    for row in cur.execute("SELECT customers FROM Blocks WHERE block_id=?", (block_id,)):
        customers = row
    connect.close()
    return customers


def inc_customers_block(block_id):
    old_value = get_customers_block(block_id)
    new_value = old_value[0] + 1
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    cur.execute("UPDATE Blocks SET customers=? WHERE block_id=?", (new_value, block_id,))
    connect.commit()
    connect.close()
    return (True)


def get_all_data_block(block_id):
    all_data = []
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in cur.execute("SELECT * FROM Blocks WHERE block_id=?", (block_id,)):
        all_data.append(row)
    return (all_data)


def get_all_blocks_data():
    all_block_data = []
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in cur.execute("SELECT * FROM Blocks"):
        all_block_data.append(row)
    return (all_block_data)
def get_all_block_data(block_name):
    all_block_data = []
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in cur.execute("SELECT * FROM Blocks where block_name=?",(block_name,)):
        all_block_data.append(row)
    return (all_block_data)


def update_block(block_name,new_data):
    old_data=get_all_block_data(block_name)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    cur.execute('''UPDATE Blocks
                SET black_name=? , comport=? WHERE block_name=?''', (new_data[0], new_data[1], block_name))
    connect.commit()
    connect.close()
# Customer block data part
# Contains functions about the customer

def check_customer_duplicity(name, block_name):
    block_id = get_block_id(block_name)
    # print(block_name)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    # print(block_id)
    cur = connect.cursor()
    check = False
    for row in cur.execute("SELECT customer_name FROM Customers WHERE block_id=?", (block_id[0],)):
        if row[0] == name:
            # print('name already exists')
            check = True
            break
        else:
            print('customer already exists')
            continue
    return (check)


# data=[Customer_name,block_name,GST_Number,address,ph_no,email_id]
def add_customer(data):
    block_id = get_block_id(data[1])
    customers = 0
    check = check_customer_duplicity(data[0], data[1])
    # print(block_id)
    if check == False:
        connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
        cur = connect.cursor()
        first = '''INSERT INTO Customers (
                 customer_name,
                 block_id,
                 GST_Number,
                 address,
                 ph_no,
                 email_id,
                 meters
                   )
                VALUES
                 (?,?,?,?,?,?,?)'''

        cur.execute(first, (data[0], block_id[0], data[2], data[3], data[4], data[5], customers))
        connect.commit()
        connect.close()
    # print(block_id)
    inc_customers_block(block_id[0])


def get_all_customer_names():
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    names = []
    for row in cur.execute("SELECT customer_name FROM Customers"):
        names.append(row)
    connect.close()
    return names


def get_customer_id(name, block_name):
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    block_id = get_block_id(block_name)
    customer_id = 'Null'
    for row in cur.execute("SELECT customer_id FROM Customers WHERE customer_name=? AND block_id=?",
                           (name, block_id[0],)):
        customer_id = row
    connect.close()
    return (customer_id)


def get_meters_customer(customer_id):
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    meters = 'Null'
    for row in cur.execute("SELECT meters FROM Customers WHERE customer_id=?", (customer_id,)):
        meters = row
    connect.close()
    return (meters)


def inc_meters_customer(customer_id):
    old_value = get_meters_customer(customer_id[0])
    #print(old_value)
    new_value = old_value[0] + 1
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    cur.execute("UPDATE Customers SET meters=? WHERE customer_id=?", (new_value, customer_id[0],))
    connect.commit()
    connect.close()
    return (True)


def get_all_block_customer(block_name):
    pass


def get_all_customers_data():
    all_customer_data = []
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in cur.execute("SELECT * FROM Customers"):
        all_customer_data.append(row)
    return (all_customer_data)

def get_all_customer_data(data_id):
    customer_id=get_customer_id(data_id[0],data_id[1])
    all_customer_data=[]
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in cur.execute("SELECT * FROM Customers WHERE customer_id=?",(customer_id[0],)):
        all_customer_data.append(row)
    return (all_customer_data)

#data_id=[customer_name,block_name]
#new_id=[customer_name,GST_Number,'address',ph_no,email_id]
def update_customer(data_id,new_data):
    customer_id=get_customer_id(data_id[0],data_id[1])
    old_data=get_all_customer_data(data_id)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    cur.execute('''UPDATE Customers
                SET customer_name=? , GST_Number=?, address=?,ph_no=?,email_id=? WHERE customer_id=?''', (new_data[0], new_data[1], new_data[2], new_data[3],new_data[4], custometer_id[0]))
    connect.commit()
    connect.close()

def check_meter_duplicity(name, customer_name, block_name):
    customer_id = get_customer_id(customer_name, block_name)
    # print(block_name)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    # print(block_id)
    cur = connect.cursor()
    check = False
    for row in cur.execute("SELECT meter_name FROM Meters WHERE customer_id=?", (customer_id[0],)):
        if row[0] == name:
            # print('name already exists')
            check = True
            break
        else:
            print('meter already exists')
            continue
    return (check)

def get_all_slave_id():
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    slave_id=[]
    for row in cur.execute("SELECT slave_id FROM Meters"):
        slave_id.append(row)
    return slave_id

# data=[meter_name,slave_id,baud_rate,parity,stop_bit,time_out,customer_name,block_name]

def add_meter(data):
    check=check_meter_duplicity(data[0], data[6], data[7])
    customer_id = get_customer_id(data[6], data[7])
    slaveids = get_all_slave_id()
    parameters = 0
    if check==False:
        if data[1] in slaveids:
            #print('slave_id_already_exists')
            return False
        else:
            connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
            cur = connect.cursor()
            first = '''INSERT INTO Meters (
                     meter_name,
                     slave_id,
                     baud_rate,
                     parity,
                     stop_bit,
                     time_out,
                     customer_id,
                     parameters
                       )
                    VALUES
                     (?,?,?,?,?,?,?,?)'''

            cur.execute(first, (data[0], data[1], data[2], data[3], data[4], data[5], customer_id[0], parameters))
            connect.commit()
            connect.close()
            inc_meters_customer(customer_id)
        return True
    else:
        return False

def get_all_meter_names():
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    names = []
    for row in cur.execute("SELECT meter_name FROM Meters"):
        names.append(row)
    connect.close()
    return (names)


def get_meter_id(name, customer_name, block_name):
    customer_id=get_customer_id(customer_name, block_name)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    meter_id = 'Null'
    for row in cur.execute("SELECT meter_id FROM Meters WHERE meter_name=? AND customer_id=?", (name,customer_id[0],)):
        meter_id = row
    connect.close()
    return (meter_id)


def get_parameters_meter(meter_id):
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    parameters = 'Null'
    for row in cur.execute("SELECT parameters FROM Meters WHERE meter_id=?", (meter_id,)):
        parameters = row
    connect.close()
    return (parameters)


def inc_parameters_meter(meter_id):
    old_value = get_parameters_meter(meter_id)
    new_value = old_value[0] + 1
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    cur.execute("UPDATE Meters SET parameters=? WHERE meter_id=?", (new_value, meter_id,))
    connect.commit()
    connect.close()
    return (True)


def get_all_meters_data():
    all_meter_data = []
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in cur.execute("SELECT * FROM  Meters"):
        all_meter_data.append(row)
    return (all_meter_data)


def get_meter_data(data_id):
    meter_id = get_meter_id(data_id[0], data_id[1], data_id[2])
    data=[]
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in cur.execute("SELECT * FROM Meters WHERE meter_id=? ",(meter_id[0],)):
        data.append(row)

#data_id=[meter_name,customer_name,block_name]
#new_data=[meter_name,baud_rate,parity,stop_bit,time_out]
def update_meter(data_id,new_data):
    meter_id=get_meter_id(data_id[0],data_id[1],data_id[2])
    old_data=get_meter_data(data_id)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    cur.execute('''UPDATE Meters
                SET meter_name=?, slave_id=?  , baud_rate=?, parity=?,stop_bit=?,time_out=? WHERE meter_id=?''', (new_data[0], new_data[1], new_data[2], new_data[3],new_data[4],new_data[5],  meter_id[0]))
    connect.commit()
    connect.close()



def check_parameter_duplicity(name, meter_name, customer_name, block_name):
    meter_id = get_meter_id(meter_name,customer_name, block_name)
    # print(block_name)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    # print(block_id)
    cur = connect.cursor()
    check = False
    for row in cur.execute("SELECT param_name FROM Parameters WHERE meter_id=?", (meter_id[0],)):
        if row[0] == name:
            # print('name already exists')
            check = True
            break
        else:
            continue
    return (check)


# data=[param_name,param_type,param_address,log_rate,log_unit,meter_name,customer_name,block_name]
def add_parameter(data):
    check=check_parameter_duplicity(data[0],data[5],data[6],data[7])
    if check==False:
        connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
        cur = connect.cursor()
        meter_id = get_meter_id(data[5],data[6],data[7])
        first = '''INSERT INTO Parameters (
                 param_name,
                 param_type,
                 param_address,
                 log_rate,
                 log_unit,
                 meter_id
                   )
                VALUES
                 (?,?,?,?,?,?)'''

        cur.execute(first, (data[0], data[1], data[2], data[3], data[4], meter_id[0]))
        connect.commit()
        connect.close()
        inc_parameters_meter(meter_id[0])
    else:
        print('Parameter already Exists')
def get_param_id(name,meter_name,customer_name,block_name):
    meter_id=get_meter_id(meter_name,customer_name,block_name)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    print(meter_id)
    cur = connect.cursor()
    for row in cur.execute("SELECT param_id FROM Parameters WHERE meter_id=? AND param_name=?",(meter_id[0],name,)):
        return(row)


#data_id=[para_name,meter_name,customer_name,block_name]
def get_all_parameter_data(data_id):
    parameter_id = get_param_id(data_id[0], data_id[1], data_id[2], data_id[3])
    print (parameter_id)
    data=[]
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in cur.execute("SELECT * FROM Parameters WHERE param_id=? ",(parameter_id[0],)):
        data.append(row)
#new_data=[param_name,para_type,para_address,log_rate,log_unit]

def update_parameter(data_id,new_data):
    parameter_id=get_param_id(data_id[0],data_id[1],data_id[2],data_id[3])
    old_data=get_all_parameter_data(data_id)
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    cur.execute('''UPDATE Parameters
                SET param_name=?, param_type=?  , param_address=?, log_rate=?,log_unit=? WHERE param_id=?''', (new_data[0], new_data[1], new_data[2], new_data[3],new_data[4], parameter_id[0]))
    connect.commit()
    connect.close()

def add_logging(data):
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    first = '''INSERT INTO Parameters (
             param_name,
             param_type,
             param_address,
             log_rate,
             log_unit,
             meter_id
               )
            VALUES
             (?,?,?,?,?,?)'''

    cur.execute(first, (data[0], data[1], data[2], data[3], data[4], data[5]))
    connect.commit()
    connect.close()


def db_init():
    connect = sqlite3.connect('C:\Python27\gui_modbus\EMS\data\data_base_files\data.db')
    cur = connect.cursor()
    for row in curr.execute("SELECT * FROM Blocks"):
        if row==None:
            data_block = ['Block', 0, 'Select COM Port']
            add_block(data_block)

    for row in curr.execute("SELECT * FROM Customers"):
        if row==None:
            data=['Customer','Block','GST_Number','address','ph_no','email_id']

#    for meter in

# block_names = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh']
# for i in range(0, len(block_names)):
#     data_block = [block_names[i], 0, 'COM7']
#     add_block(data_block)

# customer_names = ['one', 'two', 'three']
#

# for i in range(0,len(block_names)):
#     for j in range(0,len(customer_names)):
#         data1=[customer_names[j],block_names[i],'GST_Number','address','ph_no','email_id']
#         add_customer(data1)
#
# meter_names=['met1','met2','met3','met4','met5']
# slave_id=0
# for i in range(0,len(block_names)):
#      for j in range(0,len(customer_names)):
#          for k in range(0,len(meter_names)):
#              slave_id=slave_id+1
#              data2=[meter_names[k],slave_id,9600,'Even',1,8,customer_names[j],block_names[i]]
#              add_meter(data2)
# dataparam=['param_name','param_type',1000,24,'log_unit','met1','one','first']
# add_parameter(dataparam)
# dataparam=['param_name1','param_type',1000,24,'log_unit','met1','one','first']
# add_parameter(dataparam)
# dataparam=['param_name2','param_type',1000,24,'log_unit','met1','one','first']
# add_parameter(dataparam)
# #print(get_param_id('param_name','met1','one','first'))

