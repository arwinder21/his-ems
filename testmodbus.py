import minimalmodbus
import serial
import serial.tools.list_ports as list_ports
import time
import threading
import apis
import time,datetime
import schedule



a=apis.get_keys()
timevalue=str(a[0][2])+':'+str(a[0][3])
comlist = serial.tools.list_ports.comports()
connected = []

for element in comlist:
    connected.append(element.device)

print(connected)

def initiate(connected,slaveid):
    #initiates the code
    inst=minimalmodbus.Instrument(connected, slaveid)
    inst.serial.baudrate = 9600
    inst.serial.bytesize = 8
    inst.serial.parity=serial.PARITY_NONE
    inst.serial.stopbits = 1
    inst.serial.timeout = 1
    inst.serial.close_after_each_call = True
    inst.handle_local_ecgo = True
##    inst.baudrate = 9600
##    inst.bytesize = 8
##    inst.parity=serial.PARITY_NONE
##    inst.stopbits = 1
##    inst.timeout = 5
##    inst.close_after_each_call = True
    return(inst)
meters=[]

def initiate_all(num,slave_id,connected):
    global meters
    for i in range(0,num):
        meters.append(initiate(connected,slave_id[i]))
    return(meters)

def read_float_all(meter):
    values=[]
    for i in range(1,33):
        #time.sleep(0.1)
        a=meter.read_float(2*i-1,4,2)
        print(a)
        values.append(a)
        
    return(values)

##def read_float_all(meter):
##    values=[]
##    values.append(meter._genericCommand(4,0,20))       
##    return(values)

def read_kwh(meter,s_id):
    global all_values
    values=all_values[str(s_id)][29]
    return(values)

def read_kva(meter):
    try:
        values=(meter.read_float(45,4,2))
    except:
        values=0
    return(values)

def read_kvh(meter):
    try:
        values=(meter.read_float(61,4,2))
    except:
        values=0
    return(values)

def read_pf(meter):
    try:
        values=(meter.read_float(55,4,2))

    except:
        values=0
    return(values)


slave_id=apis.get_all_slave_id()
print(slave_id)
c=initiate_all(26,slave_id,connected[0])
#c=initiate_all(26,slave_id,'connected')
all_values={}
all_threads=[]
value_present=False

for i in slave_id:
    all_values[str(i)]='NULL'

def add_start_kwh():
    meter_id=1
    print('add_start_kwh')
    while meter_id<29:
        s_id=apis.slave_id_from_mid(meter_id)
        meter_obj=c[s_id-1]
        #try:
        time.sleep(1)
        energy=(read_kwh(meter_obj,s_id))
        print(energy)
        apis.add_start_values(meter_id,energy)
        meter_id+=1
        print('value added of meter id:'+str(meter_id-1))
##        except:
##            print('retrying meter id:'+str(meter_id))
##            time.sleep(1)
##            add_start_kwh()
            
            

def add_start_kwh_daily():
    meter_id=1
    while meter_id<29:
        s_id=apis.slave_id_from_mid(meter_id)
        meter_obj=c[s_id-1]
        try:
            time.sleep(1)
            energy=(read_kwh(meter_obj,s_id))
            print(energy)
            apis.add_start_values_daily(meter_id,energy)
            meter_id+=1
            print('value added of meter id:'+str(meter_id-1))
        except:
            print('retrying meter id:'+str(meter_id))
            time.sleep(1)
            add_start_kwh_daily()
           
 
            



def add_end_kwh():
    meter_id=1
    while meter_id<29:
        s_id=apis.slave_id_from_mid(meter_id)
        meter_obj=c[s_id-1]
        try:
            time.sleep(1)
            energy=(read_kwh(meter_obj,s_id))
            apis.add_end_values(meter_id,energy)
            meter_id+=1
            print(energy)
            print('value added of meter id:'+str(meter_id-1))
        except:
            print('retrying meter id:'+str(meter_id))
            time.sleep(1)
            add_end_kwh()
            

def add_end_kwh_daily():
    meter_id=1
    while meter_id<29:
        s_id=apis.slave_id_from_mid(meter_id)
        meter_obj=c[s_id-1]
        
        time.sleep(1)
        energy=(read_kwh(meter_obj,s_id))
        apis.add_end_values_daily(meter_id,energy)
        meter_id+=1
        print(energy)
        print('value added of meter id:'+str(meter_id-1))
        
##            print('retrying meter id:'+str(meter_id))
##            time.sleep(1)
##            add_start_kwh()

def report_daily():
    print('generating daily report')
    add_end_kwh_daily()
    print('end_values_added')
    apis.generate_daily_kwh_rpt()
    add_start_kwh_daily()
    datenow=datetime.datetime.now()
    date=datenow.day
    if date==1:
        report_monthly()
    else:
        print("same_month")

def report_daily_thread():
    thread_repd=threading.Thread(target=apis.generate_manual_report)
    thread_repd.start()



##def report_daily_kpa():
##    thread_repk=threading.Thread(target=report)
##    thread_repk.start()

def report_monthly():
    apis.generate_report_monthly()
    

def get_values():
    global c,all_values,slave_id,value_present
    while(1):
        for i in range(len(slave_id)):
            #print(all_values)
            #print(i)
            try:
                all_values[str(slave_id[i])]=read_float_all(c[slave_id[i]-1])
               # print(all_values)
            except:
                print('exception in getting values')

        

def get_value_once():
    global all_values
    for i in range(len(slave_id)):
        #print(all_values)
        #print(i)
        all_values[str(slave_id[i])]=read_float_all(c[slave_id[i]-1])
        #print(all_values)


        
        

def fetch_values():
    global all_values
    
    return(all_values)
            #print(all_values)



def thread_job():
    while 1:
        schedule.run_pending()
        time.sleep(60)

print(timevalue)

schedule.every().day.at(timevalue).do(report_daily)
mdi=0
reporting=0

def set_report():
    global reporting
    reporting=1

def set_report_kpi():
    global reporting
    reporting=2
    print(reporting)
#get_value_once()

def main_func():
    global all_values,mdi,reporting
    get_value_once()  #update all the values of parameters
    print(all_values)
    add_start_kwh()   #add start kwh values
    add_start_kwh_daily()   #add start kwh values 
    date_now=datetime.datetime.now().day    # get current date 
    month_now=datetime.datetime.now().month  # get current month
    while (1):
        total_sum=0        #sum variable of mdi
        pf_sum=0         
        pf_count=0
        count=0
        m_name='Mains'      
        slave=apis.slave_id_from_mname(m_name)
        meter_obj=c[slave-1]
        start_time=time.time()
        while(1):
            now_time=time.time()
            elapsed=int(now_time)-int(start_time)
            #print(elapsed)
            if elapsed<30:
                try:
                    kva=read_kva(meter_obj)
                    pf=read_pf(meter_obj)
                    pf_sum+=abs(pf)
                    pf_count+=1
                    total_sum+=float(abs(kva))
                    count+=1
                except:
                    pass
            else:
                mdi_temp=total_sum/count
                get_value_once()
                dg_values=apis.get_dg_values()
                dg1=all_values['1']
                dg2=all_values['23']
                try:
                    if int(dg1[11])>1:
                        dg1rh=float(dg_values[0])
                        dg1rh+=(0.84)
                        apis.add_dg1rh(dg1rh)
                except:
                    pass
                try:
                    if int(dg2[11])>1:
                        dg2rh=float(dg_values[1])
                        dg2rh+=(0.84)
                        apis.add_dg2rh(dg2rh)
                except:
                    pass
                for i in range(0,28):
                    ss=apis.slave_id_from_mid(str(i+1))
                    apis.insert_values_meter(i,(all_values[str(ss)][0:30]+[mdi_temp]))
                try:
                    apis.add_pf_values(abs(pf),pf_count)
                except:
                    pass
                if mdi_temp>mdi:
                    mdi=mdi_temp
                    try:
                        apis.add_mdi(mdi)
                    except:
                        pass
                else:
                    pass
                #start_time=time.time()

                #curr_day=datetime.datetime.now().day
                #curr_month=datetime.datetime.now().month
                #print(curr_day)
##                print(date_now)
##                if curr_month==month_now:
##                    pass
##                else:
##                    report_monthly()
##                    month_now=datetime.datetime.mow().month

                    
def get_mdi():
    global mdi
    mdi=float(int(float(apis.get_mdi())*1000))/1000
    return(mdi)

#report_daily()
##report_daily()

#report()
while(1):
    get_value_once()
##report_daily()    

##report_daily()
