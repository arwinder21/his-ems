import minimalmodbus
import serial
import serial.tools.list_ports as list_ports
import time
import threading
comlist = serial.tools.list_ports.comports()
connected = []
for element in comlist:
    connected.append(element.device)

def fetch_departments():
    departments={'D1':{'meters':'3','head_name':'H1','head_no':'9867684937','head_email':'h1@h1.com'},\
                 'D2':{'meters':'4','head_name':'H1','head_no':'9867684937','head_email':'h1@h1.com'},\
                 'D3':{'meters':'5','head_name':'H1','head_no':'9867684937','head_email':'h1@h1.com'},\
                 'D4':{'meters':'7','head_name':'H1','head_no':'9867684937','head_email':'h1@h1.com'},\
                 'D5':{'meters':'9','head_name':'H1','head_no':'9867684937','head_email':'h1@h1.com'}}
    return(departments)
def fetch_meters():
    meters={'M1':{'slave_id':'1','dept_name':'D1','type':'input'},'M2':{'slave_id':'1','dept_name':'D3','type':'output'},'M3':{'slave_id':'1','dept_name':'D4','type':'output'},'M4':{'slave_id':'1','dept_name':'D5','type':'output'},\
            'M5':{'slave_id':'1','dept_name':'D1','type':'input'},'M6':{'slave_id':'1','dept_name':'D3','type':'output'},'M7':{'slave_id':'1','dept_name':'D4','type':'output'},'M8':{'slave_id':'1','dept_name':'D5','type':'output'},\
            'M9':{'slave_id':'1','dept_name':'D1','type':'input'},'M12':{'slave_id':'1','dept_name':'D3','type':'output'},'M15':{'slave_id':'1','dept_name':'D4','type':'output'},'M18':{'slave_id':'1','dept_name':'D5','type':'output'},\
            'M10':{'slave_id':'1','dept_name':'D2','type':'input'},'M13':{'slave_id':'1','dept_name':'D3','type':'output'},'M16':{'slave_id':'1','dept_name':'D4','type':'output'},'M19':{'slave_id':'1','dept_name':'D5','type':'output'},\
            'M11':{'slave_id':'1','dept_name':'D2','type':'input'},'M14':{'slave_id':'1','dept_name':'D3','type':'output'},'M17':{'slave_id':'1','dept_name':'D4','type':'output'},'M20':{'slave_id':'1','dept_name':'D5','type':'output'},\
            'M21':{'slave_id':'1','dept_name':'D2','type':'input'},'M23':{'slave_id':'1','dept_name':'D4','type':'output'},'M25':{'slave_id':'1','dept_name':'D5','type':'output'},'M27':{'slave_id':'1','dept_name':'D5','type':'output'},\
            'M22':{'slave_id':'1','dept_name':'D2','type':'input'},'M24':{'slave_id':'1','dept_name':'D4','type':'input'},'M26':{'slave_id':'1','dept_name':'D5','type':'output'},'M28':{'slave_id':'1','dept_name':'D5','type':'output'}}
    return(meters)

    
def get_serial_port():
    comlist = serial.tools.list_ports.comports()
    connected = []
    for element in comlist:
        connected.append(element.device)

class modbus():
    def __init__(self,connected,slave_id):
        self._connected=connected[0]
        self._slave_id=slave_id

    def initiate(self,baudrate,bytesize,parity,stopbits,timeout,close):
        if parity=="even":
            self._parity=serial.PARITY_EVEN
        if parity=="odd":
            self._parity=serial.PARITY_ODD
        if parity=="none":
            self._parity=serial.PARITY_NONE
        self._baudrate=baudrate
        self._bytesize=bytesize
        self._stopbits=stopbits
        self._timeout=timeout
        self._close=close
        self._ins=minimalmodbus.Instrument(self._connected, self._slave_id)
        self._ins.serial.baudrate = self._baudrate
        self._ins.serial.bytesize = self._bytesize
        self._ins.serial.parity = self._parity
        self._ins.serial.stopbits = self._stopbits
        self._ins.serial.timeout = self._timeout
        self._ins.serial.close_after_each_call = self._close
       
        return(self._ins)

    def read_all_float(self,regs):
        values=[]
        #attributes = inspect.getmembers(MyClass, lambda a:not(inspect.isroutine(a)))
        #print("hereh")
        #print(self._ins)
        values=self._ins.read_float(1,4,2)
        print(values)
        time.sleep(1)
        return(values)

class dummy_modbus():
    def __init__(self,connected,slave_id):
        self._connected=connected[0]
        self._slave_id=slave_id

    def initiate(self,baudrate,bytesize,parity,stopbits,timeout,close):
        self._ins=minimalmodbus.Instrument(self._connected, self._slave_id)
        return(self._ins)

    def read_float(self,regs):
        values=[]
        #attributes = inspect.getmembers(MyClass, lambda a:not(inspect.isroutine(a)))
        #print("hereh")
        #print(self._ins)
        values=self._ins.read_all_float(3,regs)
        print(values)
        time.sleep(0.1)
        return(values)

##mod1=modbus(connected,1)
##mod2=modbus(connected,2)
##mod1.initiate(9600,8,"none",1,200,True)
##mod2.initiate(9600,8,"none",1,200,True)
##print(mod1.read_float([1,3,5]))
##print(mod2.read_float([1,3,5]))
class device_set():
    def __init__(self,num,slave_id,connected):
        self._connected=connected[0]
        self._num=num
        self._slave_id=slave_id
        self._devices=[]
        for i in range(self._num):
            self._devices.append(modbus(connected,self._slave_id[i]))

    def initiate(self,baudrate,bytesize,parity,stopbits,timeout,close):
        #print(len(self._devices))
        for j in range(0,len(self._devices)):
            #print j
            #try:
            self._devices[j].initiate(9600,8,"none",1,200,True)
            #except:
            #    continue
        return(self._devices)
    def read_values(self,regs):
        values=[]
        for j in range(len(self._devices)):
            try:
                values.append(self._devices[j].read_all_float(regs))
            except:
                values.append("NA")
            #print(self._devices[i]._ins)
        return(values)
slaveid=[]
for i in range(1,27):
    slaveid.append(i)
a=device_set(26,slaveid,connected)
a.initiate(9600,8,"none",1,200,True)
print(a.read_values([1,3,5]))
class dummy_device_set():
    global data
    def __init__(self,num,slave_id,connected):
        self._connected=connected[0]
        self._num=num
        self._slave_id=slave_id
        self._devices=[]
        for i in range(self._num):
            self._devices.append(dummy_modbus(connected,self._slave_id[i]))

    def initiate(self,baudrate,bytesize,parity,stopbits,timeout,close):
        #print(len(self._devices))
        for j in range(0,len(self._devices)):
            #print j
            self._devices[j].initiate(9600,8,"none",1,200,True)
        return(self._devices)
    def read_values(self,regs):
        values=[]
        for j in range(len(self._devices)):
            values.append(self._devices[j].read_float(regs))
            data[j]=(self._devices[j].read_float(regs))
            print(values)
            #print(self._devices[i]._ins)
        return(values)
def refresh():
    global data
    return(data)
