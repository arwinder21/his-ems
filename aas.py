def refresh():
    print('connecting to db')
    conn = sqlite3.connect('data.db')
    print('Deleting Existing meters')
    conn.execute('''DROP TABLE departments''')
    conn.execute('''DROP TABLE meters''')
    print('getting documents details')
    all_data=get_metdets()
    print('creating new tables')
    create()
    data=['','','','']
    keys=all_data.keys()
    print('populating with new data')
    for a in keys:
        temp=all_data[a]
        data[0]=str(a)
        data[1]=str(temp[0]['headname'])
        data[2]=int(temp[0]['meters'])
        data[3]=str(temp[0]['heademail'])
        insert_dept(data)
    print('getting meter details')
    data=get_met_details()
    print('poulating new meter details')
    for i in data:
        insert_meters(i)
    print('refreshing done')



def send_value(dept,mid,v):
    try:
        url3='http://18.218.111.25/DEMS/api/formLoggings.php'
        #print((v)
        values1={"Department":str(dept),"DeviceId":int(mid),"V1":str(float(int(v[0])*100)/100),"V2":str(float(int(v[1])*100)/100),"V3":str(float(int(v[2]*100))/100),
            "VAvg":str(float(int(v[4])*100)/100),
            "V12":str(float(int(v[5]*100))/100),
            "V23":str(float(int(v[6]*100))/100),
            "V31":str(float(int(v[7]*100))/100),
            "VAAvg":str(float(int(v[8]*100))/100),
            "I1":str(float(int(v[9]*100))/100),
            "I2":str(float(int(v[10]*100))/100),
            "I3":str(float(int(v[11]*100))/100),
            "IAvg":str(float(int(v[12]*100))/100),
            "kW1":str(float(int(v[13]*100))/100),
            "kW2":str(float(int(v[14]*100))/100),
            "kW3":str(float(int(v[15]*100))/100),
            "kWAvg":str(float(int(v[22]*100))/100),
            "PF1":str(float(int(v[25]*100))/100),
            "PF2":str(float(int(v[26]*100))/100),
            "PF3":str(float(int(v[27]*100))/100),
            "PFAvg":str(float(int(v[28]*100))/100),
            "ActivePower":str(float(int(v[30]*100))/100),
            "ReactivePower":str(float(int(v[31]*100))/100),
            "MDI":str(float(int(v[32]*100))/100)}

        r8= requests.post(url3,data=values1)
        print(r8.text)
    except:
        url3='http://18.218.111.25/DEMS/api/formLoggings.php'
        #print((v)
        values1={"Department":str(dept),"DeviceId":int(mid),"V1":str(v[0]),"V2":str(v[1]),"V3":str(v[2]),
            "VAvg":str(v[4]),
            "V12":str(v[5]),
            "V23":str(v[6]),
            "V31":str(v[7]),
            "VAAvg":str(v[8]),
            "I1":str(v[9]),
            "I2":str(v[10]),
            "I3":str(v[11]),
            "IAvg":str(v[12]),
            "kW1":str(v[13]),
            "kW2":str(v[14]),
            "kW3":str(v[15]),
            "kWAvg":str(v[22]),
            "PF1":str(v[25]),
            "PF2":str(v[26]),
            "PF3":str(v[27]),
            "PFAvg":str(v[28]),
            "ActivePower":str(v[30]),
            "ReactivePower":str(v[31]),
            "MDI":str(v[32])}

        r8= requests.post(url3,data=values1)
        print(r8.text)
headers={"Content-Type":'application/json',"Accept":'application/json'}
headers1={"Content-Type":'application/json'}
def get_meternames():
    global headers
    allmeternames=[]
    slave_meter={}
    meter_slave={}
    url3='http://18.218.111.25/DEMS/api/OnlyMeterDetails.php'
    r3 = requests.get(url3,headers=headers)
    b=json.loads(r3.text)
    b=b["data"]
    for i in range(len(b)):
        temp=b[i]
        allmeternames.append(str(temp.keys()[0]))
        #print(temp['PSPCL']['Slaveid'])
        slave_meter[int(temp[(temp.keys()[0])]['Slaveid'])]=str(temp.keys()[0])
        meter_slave[allmeternames[i]]=int(slave_meter.keys()[i])
    return(allmeternames,slave_meter,meter_slave)

def get_deptnames():
    global headers
    alldeptnames=[]
    url1 = 'http://18.218.111.25/DEMS/api/DepartmentName.php'
    r1 = requests.get(url1,headers=headers)
    a=json.loads(r1.text)
    a=a["DepartmentName"]
    for i in range(len(a)):
        temp=a[i]
        alldeptnames.append(str(temp["DepartmentName"]))    
    return(alldeptnames)

def get_metdept():
    alldeptnames=get_deptnames()
    meter_depts={}
    metdeptmap={}
    url2 = 'http://18.218.111.25/DEMS/api/MeterName2.php?Department='
    for i in alldeptnames:
        r2 = requests.get(url2+str(i),headers=headers)
        temp=json.loads(r2.text)
        temp1=temp['data']
        metertemp=[]
        for j in temp1:
            metertemp.append(j['MeterName'])
            metdeptmap[j['MeterName']]=str(i)
        meter_depts[str(i)]=metertemp
        metertemp=[]
    return(meter_depts,metdeptmap)


def get_metdets():
    alldeptnames=get_deptnames()
    meter_depts={}
    url3 = 'http://18.218.111.25/DEMS/api/GetSingleDepartmentDetails.php?Department='
    for i in alldeptnames:
        r3 = requests.get(url3+str(i),headers=headers)
        temp=json.loads(r3.text)
        temp1=temp['departments']
        metertemp=[]
        for j in temp1:
            metertemp.append(j[str(i)])
        meter_depts[str(i)]=metertemp
        metertemp=[]
    return(meter_depts)    
