from functools import partial
from tkinter import *
import serial as serial1
import serial.tools.list_ports as list_ports
import helpfunctions

def dummy(i):
    print(i)

 

count=0    
def inc_all():
    global paramdata,count
    for i in range(0,len(paramdata)):
        paramdata[i].config(state='normal')
        paramdata[i].delete(0,END)
        paramdata[i].insert(0,str(count))
        paramdata[i].config(state='readonly')
    count=count+1
    root.after(200,inc_all)
        
root=Tk()
root.title("EM Soft ")
root.geometry("{0}x{1}+0+0".format(root.winfo_screenwidth(), root.winfo_screenheight()))
root.config(background='blue')
main_frame=Frame(root,height=root.winfo_screenheight()/10,width=root.winfo_screenwidth(),bg='light green')
devframe=Frame(root,width=root.winfo_screenwidth(),height=9*root.winfo_screenheight()/10)
settingframe=Frame(devframe,width=root.winfo_screenwidth()/6,height=9*root.winfo_screenheight()/10,bg='light blue')
dataframe=Frame(devframe,width=2*root.winfo_screenwidth()/3,height=9*root.winfo_screenheight()/10,bg='white')
secondsetframe=Frame(devframe,width=root.winfo_screenwidth()/6,height=9*root.winfo_screenheight()/10,bg='light blue')
main_frame.pack()
settingframe.grid(row=0,column=0,sticky='w')
dataframe.grid(row=0,column=1,sticky='e')
secondsetframe.grid(row=0,column=2,sticky='w')
devframe.pack()
main_frame.pack_propagate(0)
devframe.grid_propagate(0)
settingframe.grid_propagate(0)
dataframe.grid_propagate(0)
secondsetframe.grid_propagate(0)
title=Label(main_frame,text='Energy Management System',font=("comic sans ms", 44),bg='light green')
#title=Label(main_frame,text='Energy Management System',font=("Time", 44),bg='light green')
#title=Label(main_frame,text='Energy Management System',font=("courier", 44),bg='light green')
#title=Label(main_frame,text='Energy Management System',font=("fixedsys", 44),bg='light green')
#title=Label(main_frame,text='Energy Management System',font=("ms sans serif", 44),bg='light green')
#title=Label(main_frame,text='Energy Management System',font=("ms serif", 44),bg='light green')
#title=Label(main_frame,text='Energy Management System',font=("symbol", 44),bg='light green')
#title=Label(main_frame,text='Energy Management System',font=("system", 44),bg='light green')
#title=Label(main_frame,text='Energy Management System',font=("verdana", 44),bg='light green')
title.pack()



paramlbls=[]
unitlbls=[]
paramdata=[]
editbtns=[]


for i in range(0,10):
    paramvar='Parameter-'+str(i+1)
    paramlbls.append(Label(dataframe,text=paramvar,font=("Times", 20),bg='white',width=20))
    unitlbls.append(Label(dataframe,text='unit',font=("Times", 20),bg='white'))
    paramdata.append(Entry(dataframe,state='readonly',width=10))
    editbtns.append(Button(dataframe,text='Edit',width=20,command=lambda i=i:helpfunctions.edit_parameter(paramlbls,i)))
    paramdata[i].grid(row=i,column=1,pady=5,padx=15,ipadx=15,sticky='w')
    paramlbls[i].grid(row=i,column=0,pady=5,padx=15,ipadx=15)
    unitlbls[i].grid(row=i,column=2,pady=5,padx=15,ipadx=15)
    editbtns[i].grid(row=i,column=3,pady=5,padx=15,sticky='w')

editmeter=Button(settingframe,text='Edit Meter',width=20,font=("Times", 15),command=lambda:dummy(1))
editmeter.grid(row=0,column=0)
editcust=Button(settingframe,text='Edit Customer',width=20,font=("Times", 15),command=lambda:dummy(1))
editcust.grid(row=1,column=0)
connect=Button(settingframe,text='Connect',width=20,font=("Times", 15),command=lambda:dummy(1))
connect.grid(row=2,column=0)
disconnect=Button(settingframe,text='Disconnect',width=20,font=("Times", 15),command=lambda:dummy(1))
disconnect.grid(row=3,column=0)
export=Button(settingframe,text='Export',width=20,font=("Times", 15),command=lambda:dummy(1))
export.grid(row=4,column=0)
iot=Button(secondsetframe,text='IoT',width=20,font=("Times", 15),command=lambda:dummy(1))
iot.grid(row=0,column=0)
inc_all()
root.mainloop()
