import schedule
import time

def joba():
    print("I'm working...a")
def jobb():
    print("I'm working...b")    
def jobc():
    print("I'm working...c")

def jobd():
    print("I'm working...d")
def jobe():
    print("I'm working...e")
def jobf():
    print("I'm working...f")
schedule.every(10).minutes.do(joba)
schedule.every().hour.do(jobb)
schedule.every().day.at("14:40").do(jobc)
schedule.every().monday.do(jobd)
schedule.every().wednesday.at("13:15").do(jobe)
schedule.every().minute.at(":17").do(jobf)

while True:
    schedule.run_pending()
    time.sleep(1)
